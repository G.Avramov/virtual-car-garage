﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.MVC.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class OrdersAPIController : ControllerBase
    {
        private readonly IOrderService orderService;

        public OrdersAPIController(IOrderService orderService)
        {
            this.orderService = orderService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                OrderDTO order = await this.orderService.GetAsync(id);
                return Ok(order);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                List<OrderDTO> orders = await this.orderService.GetAllAsync();
                return Ok(orders);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> Create([FromBody] OrderDTO order)
        {
            try
            {
                bool created = await this.orderService.CreateAsync(order);
                return Created("post", order);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await this.orderService.DeleteAsync(id);
                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("")]
        public async Task<IActionResult> Update([FromBody] OrderEditDTO order)
        {
            try
            {
                bool update = await this.orderService.UpdateAsync(order);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }


        [HttpPut("AddServices/{id}")]
        public async Task<IActionResult> AddServices(int id, [FromBody] List<ServiceDTO> servicesDTO)
        {
            try
            {
                bool update = await this.orderService.AddServices(id, servicesDTO);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("RemoveServices/{id}")]
        public async Task<IActionResult> RemoveServices(int id, [FromBody] List<ServiceDTO> servicesDTO)
        {
            try
            {
                bool update = await this.orderService.RemoveServices(id, servicesDTO);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
