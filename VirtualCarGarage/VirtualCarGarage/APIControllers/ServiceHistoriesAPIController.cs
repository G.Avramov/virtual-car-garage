﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.MVC.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class ServiceHistoriesAPIController : ControllerBase
    {
        private readonly IServiceHistoryService serviceHistoryService;

        public ServiceHistoriesAPIController(IServiceHistoryService serviceHistoryService)
        {
            this.serviceHistoryService = serviceHistoryService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                ServiceHistoryDTO serviceHistory = await this.serviceHistoryService.GetAsync(id);
                return Ok(serviceHistory);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("GetByUser/{id}")]
        public async Task<IActionResult> GetByUserIdAsync(int id)
        {
            try
            {
                List<ServiceHistoryDTO> serviceHistories = await this.serviceHistoryService.GetByUserIdAsync(id);
                return Ok(serviceHistories);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                List<ServiceHistoryDTO> serviceHistories = await this.serviceHistoryService.GetAllAsync();
                return Ok(serviceHistories);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> Create([FromBody] ServiceHistoryDTO serviceHistory)
        {
            try
            {
                bool created = await this.serviceHistoryService.CreateAsync(serviceHistory);
                return Created("post", serviceHistory);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await this.serviceHistoryService.DeleteAsync(id);
                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("")]
        public async Task<IActionResult> Update([FromBody] ServiceHistoryDTO serviceHistory)
        {
            try
            {
                bool update = await this.serviceHistoryService.UpdateAsync(serviceHistory);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
