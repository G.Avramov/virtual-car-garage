﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.MVC.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class UsersAPIController : ControllerBase
    {
        private readonly IUserService userService;

        public UsersAPIController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                UserDTO user = await this.userService.GetAsync(id);
                return Ok(user);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                List<UserDTO> users = await this.userService.GetAllAsync();
                return Ok(users);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> Create([FromBody] UserDTO user)
        {
            try
            {
                bool created = await this.userService.CreateAsync(user);
                return Created("post", user);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await this.userService.DeleteAsync(id);
                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("")]
        public async Task<IActionResult> Update([FromBody] UserDTO user)
        {
            try
            {
                bool update = await this.userService.UpdateAsync(user);
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("Changepassword/{id}")]
        public async Task<IActionResult> Put(int id, string newPassword, string confirmNewPassword)
        {
            try
            {
                bool updated = await this.userService.ChangePasswordAsync(id, newPassword, confirmNewPassword);

                return Ok();
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
