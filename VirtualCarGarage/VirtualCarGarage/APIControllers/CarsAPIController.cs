﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.MVC.APIControllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class CarsAPIController : ControllerBase
    {
        private readonly ICarService carService;

        public CarsAPIController(ICarService carService)
        {
            this.carService = carService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                CarDTO car = await this.carService.GetAsync(id);
                return Ok(car);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                List<CarDTO> cars = await this.carService.GetAllAsync();
                return Ok(cars);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("CustomerCars/{id}")]
        public async Task<IActionResult> GetAllForCustomerAsync(int id)
        {
            try
            {
                List<CarDTO> cars = await this.carService.GetAllForCustomerAsync(id);
                return Ok(cars);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("CarsWithoutServiceHistory")]
        public async Task<IActionResult> GetAllWithoutServiceHistoryAsync()
        {
            try
            {
                List<CarWithoutServiceHistoryDTO> cars = await this.carService.GetAllWithoutServiceHistoryAsync();
                return Ok(cars);
            }
            catch (Exception e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost("")]
        public async Task<IActionResult> Post([FromBody] CarDTO carDTO)
        {
            try
            {
                bool created = await this.carService.CreateAsync(carDTO);

                return Created("post", carDTO);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpPut("")]
        public async Task<IActionResult> Put([FromBody] CarDTO carDTO)
        {
            try
            {
                bool updated = await this.carService.UpdateAsync(carDTO);

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await this.carService.DeleteAsync(id);

                return NoContent();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
