﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualCarGarage.MVC.Models
{
    public class ContactUsViewModel
    {
        [Required]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} symbols.")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "{0} must be between {2} and {1} symbols.")]
        public string LastName { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 5, ErrorMessage = "{0} must be between {2} and {1} symbols.")]
        public string Email { get; set; }

        [Required]
        [StringLength(10, MinimumLength = 10, ErrorMessage = "{0} must be must be exactly 10 digits.")]
        public string PhoneNumber { get; set; }

      /*  [Required]
        [StringLength(1000, MinimumLength = 10, ErrorMessage = "{0} must be between {2} and {1} symbols.")]
        public string Message { get; set; }*/

    }
}
