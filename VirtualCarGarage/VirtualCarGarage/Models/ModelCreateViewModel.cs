﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualCarGarage.MVC.Models
{
    public class ModelCreateViewModel
    {
        public int ModelId { get; set; }
        public string Name { get; set; }
        public int ManufacturerId { get; set; }
    }
}
