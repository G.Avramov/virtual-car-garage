﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualCarGarage.MVC.Models
{
    public class CarViewModel
    {
        public int CarId { get; set; }
        public string CarIDNumber { get; set; }
        public string LicensePlate { get; set; }
        public int ModelId { get; set; }
        public int ManufacturerId { get; set; }
        public string OwnerName { get; set; }
        public int OwnerId { get; set; }
        public int YearOfManufacture { get; set; }
    }
}
