﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualCarGarage.MVC.Models
{
    public class OrderAddServicesViewModel
    {
        public int OrderId { get; set; }
        public List<ServiceViewModel> Services { get; set; }
    }
}
