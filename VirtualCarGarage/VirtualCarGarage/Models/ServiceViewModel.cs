﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualCarGarage.MVC.Models
{
    public class ServiceViewModel
    {
        public int ServiceId { get; set; }
        [Required, StringLength(100, MinimumLength = 2, ErrorMessage = "Name must be between {2} and {1}.")]
        public string Name { get; set; }
        [Required, Range(0.1, 9999.99, ErrorMessage = "Invalid Price amount! Price must be between {1} and {2}.")]
        public decimal Price { get; set; }
    }
}
