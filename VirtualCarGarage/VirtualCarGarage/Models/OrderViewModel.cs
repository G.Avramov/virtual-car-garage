﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualCarGarage.MVC.Models
{
    public class OrderViewModel
    {
        public int OrderId { get; set; }
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime OrderDate { get; set; }
        public int ServiceHistoryId { get; set; }
        public List<ServiceViewModel> ServicesOrdered { get; set; } 
    }
}
