﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualCarGarage.MVC.Models
{
    public class CarDetailsViewModel
    {
        public int CarId { get; set; }
        public string CarIDNumber { get; set; }
        public string LicensePlate { get; set; }
        public int ModelId { get; set; }
        public string ModelName { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
        public string OwnerName { get; set; }
        public int OwnerId { get; set; }
        public int YearOfManufacture { get; set; }
        public int ServiceHistoryId { get; set; }
    }
}
