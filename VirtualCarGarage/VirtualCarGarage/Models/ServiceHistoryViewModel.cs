﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualCarGarage.MVC.Models
{
    public class ServiceHistoryViewModel
    {
        public int ServiceHistoryId { get; set; }
        public int CarId { get; set; }
        public List<OrderViewModel> Orders { get; set; } 
    }
}
