﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualCarGarage.MVC.Models
{
    public class CarCreateViewModel
    {
        public int CarId { get; set; }
        [Required]
        [StringLength(17, MinimumLength = 17, ErrorMessage = "Invalid VIN Format.")]
        public string CarIDNumber { get; set; }
        [Required]
        [StringLength(10, MinimumLength = 6, ErrorMessage = "Invalid License Plate Format.")]
        public string LicensePlate { get; set; }
        [Required]
        public int ModelId { get; set; }
        [Required]
        public int OwnerId { get; set; }
        [Required]
        [Range(1950, 2022, ErrorMessage = "The field {0} must be between {1} and {2}.")]
        public int YearOfManufacture { get; set; }
    }
}
