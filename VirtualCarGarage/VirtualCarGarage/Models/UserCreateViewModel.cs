﻿using System.ComponentModel.DataAnnotations;

namespace VirtualCarGarage.MVC.Models
{
    public class UserCreateViewModel
    {
        [EmailAddress]
        [Required]
        public string Email { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Value for {0} must be between {2} and {1}.")]
        public string LastName { get; set; }
        [Required]
        [RegularExpression(@"^[0-9]{10}$", ErrorMessage = "Value for {0} must be exactly 10 digits.")]
        public string PhoneNumber { get; set; }
    }
}
