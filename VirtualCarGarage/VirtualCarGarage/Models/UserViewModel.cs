﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualCarGarage.MVC.Models
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<CarViewModel> Cars { get; set; }
        public string PhoneNumber { get; set; }
    }
}
