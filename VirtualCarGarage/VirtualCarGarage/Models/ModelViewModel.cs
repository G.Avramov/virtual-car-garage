﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VirtualCarGarage.MVC.Models
{
    public class ModelViewModel
    {
        public int ModelId { get; set; }
        [Required]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "Name must be between {2} and {1}.")]
        public string Name { get; set; }
        public int ManufacturerId { get; set; }
        [Required]
        [StringLength(25, MinimumLength = 2, ErrorMessage = "Name must be between {2} and {1}.")]
        public string ManufacturerName { get; set; }
    }
}
