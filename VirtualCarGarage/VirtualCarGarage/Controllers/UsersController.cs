﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCarGarage.Data.Models;
using VirtualCarGarage.MVC.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.MVC.Controllers
{
    [Route("[controller]")]
    [Authorize]
    public class UsersController : Controller
    {
        private readonly IUserService userService;
        private readonly UserManager<User> userManager;

        public UsersController(IUserService userService, UserManager<User> userManager)
        {
            this.userService = userService;
            this.userManager = userManager;
        }

        [HttpGet("Index")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            var users = await this.userService.GetAllAsync();

            var usersView = new List<UserViewModel>();

            foreach (var user in users)
            {
                usersView.Add(new UserViewModel
                {
                    UserId = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.Phone,
                    Email = user.Email
                });
            }

            return await Task.FromResult(View(usersView));
        }

        [HttpGet("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {

            return View();
        }

        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Email,FirstName,LastName,PhoneNumber")] UserCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var userDto = new UserDTO
                {
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Phone = model.PhoneNumber
                };
                var dto = await this.userService.CreateAsync(userDto);
                return RedirectToAction(nameof(Index));
            }
            return View(model);
        }

        [HttpGet("Edit/{id}")]
        [Authorize(Roles = "Admin, Customer")]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await this.userService.GetAsync(id);

            var userView = new UserEditViewModel
            {
                UserId = user.Id,
                Email = user.Email,
                PhoneNumber = user.Phone,
                FirstName = user.FirstName,
                LastName = user.LastName
            };            

            return View(userView);

        }

        [HttpPost("Edit/{id}")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Customer")]
        public async Task<IActionResult> Edit(UserEditViewModel user)
        {

            if (ModelState.IsValid)
            {
                var userDTO = new UserDTO
                {
                    Id = user.UserId,
                    Email = user.Email,
                    Phone = user.PhoneNumber,
                    FirstName = user.FirstName,
                    LastName = user.LastName                    
                };
                var updated = await this.userService.UpdateAsync(userDTO);
                return RedirectToAction(nameof(Index));
            }
            
            return View(user);
        }

        [HttpGet("Delete/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            var user = await this.userService.GetAsync(id);
            var userView = new UserViewModel
            {
                UserId = user.Id,
                Email = user.Email,
                PhoneNumber = user.Phone,
                FirstName = user.FirstName,
                LastName = user.LastName
            };

            return View(userView);
        }

        [HttpPost("Delete/{id}")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(UserViewModel user)
        {
            try
            {
                var result = await this.userService.DeleteAsync(user.UserId);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet("Profile")]
        [Authorize(Roles = "Admin, Customer")]
        public async Task<IActionResult> Profile()
        {
            var user = await this.userManager.GetUserAsync(User);

            var userView = new UserEditViewModel()
            {
                UserId = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber
            };

            return View(userView);
        }

        [HttpPost("Profile")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Customer")]
        public async Task<IActionResult> Profile(UserEditViewModel user)
        {
            if(ModelState.IsValid)
            {
                var userDTO = new UserDTO
                {
                    Id = user.UserId,
                    Email = user.Email,
                    Phone = user.PhoneNumber,
                    FirstName = user.FirstName,
                    LastName = user.LastName
                };
                var updated = await this.userService.UpdateAsync(userDTO);
                return View();
            }

            return View(user);
        }

        [HttpGet("ChangePass")]
        [Authorize(Roles = "Admin, Customer")]
        public async Task<IActionResult> ChangePass()
        {
            var user = await this.userManager.GetUserAsync(User);

            var changePassView = new ChangePassViewModel()
            {
                UserId = user.Id,
                NewPassword = "",
                ConfirmPassword = ""
            };

            return View(changePassView);
        }

        [HttpPost("ChangePass")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Customer")]
        public async Task<IActionResult> ChangePass(ChangePassViewModel changePassViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var user = await userManager.GetUserAsync(User);
            if (user == null)
            {
                return NotFound($"Unable to load user with ID '{userManager.GetUserId(User)}'.");
            }
          
            var changePasswordResult = await userService.ChangePasswordAsync(user.Id, changePassViewModel.NewPassword, changePassViewModel.ConfirmPassword);
                    

            return View();
        }
    }
}
