﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCarGarage.MVC.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.MVC.Controllers
{
    [Route("[controller]")]
    [Authorize(Roles = "Admin")]
    public class ServicesController : Controller
    {
        private readonly IServiceService serviceService;

        public ServicesController(IServiceService serviceService)
        {
            this.serviceService = serviceService;
        }

        [HttpGet("Index")]
        public async Task<IActionResult> Index()
        {
            var servicesDTO = await this.serviceService
               .GetAllAsync();
            var servicesView = new List<ServiceViewModel>();

            foreach (var item in servicesDTO)
            {
                servicesView.Add(new ServiceViewModel
                {
                    ServiceId = item.Id,
                    Name = item.Name,
                    Price = item.Price
                });
            }

            return View(servicesView);
        }

        [HttpGet("Create")]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Price")] ServiceCreateViewModel service)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var serviceDTO = new ServiceDTO
                    {
                        Name = service.Name,
                        Price = service.Price
                    };
                    var created = await this.serviceService.CreateAsync(serviceDTO);
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception)
                {
                    return Conflict();
                }
            }
            return View(serviceService);
        }

        [HttpGet("Edit/{id}")]
        public async Task<IActionResult> Edit(int id)
        {
            var service = await this.serviceService.GetAsync(id);

            var serviceDTO = await this.serviceService
                   .GetAsync(id);
            var serviceView = new ServiceViewModel
            {
                ServiceId = service.Id,
                Name = service.Name,
                Price = service.Price
            };

            return View(serviceView);

        }

        [HttpPost("Edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit([Bind("ServiceId,Name,Price")] ServiceViewModel service)
        {

            if (ModelState.IsValid)
            {
                var serviceDTO = new ServiceDTO
                {
                    Id = service.ServiceId,
                    Name = service.Name,
                    Price = service.Price
                };
                var updated = await serviceService.UpdateAsync(serviceDTO);
                return RedirectToAction(nameof(Index));
            }
            return View(service);
        }

        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var service = await serviceService.GetAsync(id);
            var serviceView = new ServiceViewModel
            {
                ServiceId = service.Id,
                Name = service.Name,
                Price = service.Price
            };

            return View(serviceView);
        }

        [HttpPost("Delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ServiceViewModel service)
        {
            try
            {
                var result = await serviceService.DeleteAsync(service.ServiceId);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
