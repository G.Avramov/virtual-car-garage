﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using VirtualCarGarage.Models;
using VirtualCarGarage.MVC.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUserService userService;

        public HomeController(ILogger<HomeController> logger, IUserService userService)
        {
            _logger = logger;
            this.userService = userService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("CreateFromContactForm")]
        public IActionResult CreateFromContactForm()
        {
            return View();
        }


        [HttpPost("CreateFromContactForm")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateFromContactForm([Bind("FirstName,LastName,Email,PhoneNumber")] ContactUsViewModel contactUsViewModel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var userDTO = new UserDTO
                    {
                        FirstName = contactUsViewModel.FirstName,
                        LastName = contactUsViewModel.LastName,
                        Email = contactUsViewModel.Email,
                        Phone = contactUsViewModel.PhoneNumber
                    };
                    var created = await this.userService.CreateAsync(userDTO);
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception)
                {
                    return Conflict();
                }
            }
            return View(contactUsViewModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
