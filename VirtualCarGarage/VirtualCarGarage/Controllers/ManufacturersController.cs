﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCarGarage.Data.Models;
using VirtualCarGarage.MVC.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.MVC.Controllers
{
    [Route("[controller]")]
    [Authorize(Roles = "Admin")]
    public class ManufacturersController : Controller
    {
        private readonly IManufacturerService manufacturerService;
        private readonly UserManager<User> userManager;

        public ManufacturersController(IManufacturerService manufacturerService, UserManager<User> userManager)
        {
            this.manufacturerService = manufacturerService;
            this.userManager = userManager;
        }

        [HttpGet("Index")]
        public async Task<IActionResult> Index()
        {
            var manufacturersDTO = await this.manufacturerService
               .GetAllAsync();
            var manufacturersView = new List<ManufacturerViewModel>();

            foreach (var item in manufacturersDTO)
            {
                manufacturersView.Add(new ManufacturerViewModel
                {
                    ManufacturerId = item.Id,
                    Name = item.Name
                });
            }

            return View(manufacturersView);
        }

        [HttpGet("Edit/{id}")]
        public async Task<IActionResult> Edit(int id)
        {
            var manufacturer = await this.manufacturerService.GetAsync(id);

            var manufacturerDTO = await this.manufacturerService
                   .GetAsync(id);
            var manufacturerView = new ManufacturerViewModel
            {
                ManufacturerId = manufacturer.Id,
                Name = manufacturerDTO.Name
            };

            return View(manufacturerView);

        }

        [HttpPost("Edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ManufacturerViewModel manufacturer)
        {

            if (ModelState.IsValid)
            {
                var manufacturerDTO = new ManufacturerDTO
                {
                    Id = manufacturer.ManufacturerId,
                    Name = manufacturer.Name
                };
                var modelDTO = await manufacturerService.UpdateAsync(manufacturerDTO);
                return RedirectToAction(nameof(Index));
            }
            return View(manufacturer);
        }

        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var manufacturer = await manufacturerService.GetAsync(id);
            var manufacturerView = new ManufacturerViewModel
            {
                ManufacturerId = manufacturer.Id,
                Name = manufacturer.Name
            };

            return View(manufacturerView);
        }

        [HttpPost("Delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ManufacturerViewModel manufacturer)
        {
            try
            {
                var result = await manufacturerService.DeleteAsync(manufacturer.ManufacturerId);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet("Create")]
        public IActionResult Create()
        {
            return View();
        }


        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name")] ManufacturerCreateViewModel manufacturer)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var manufacturerDto = new ManufacturerDTO
                    {
                        Name = manufacturer.Name
                    };
                    var dto = await this.manufacturerService.CreateAsync(manufacturerDto);
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception)
                {
                    return Conflict();
                }             
            }
            return View(manufacturer);
        }
    }
}
