﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCarGarage.Data.Models;
using VirtualCarGarage.MVC.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.MVC.Controllers
{
    [Route("[controller]")]
    [Authorize]
    public class CarsController : Controller
    {
        private readonly ICarService carService;
        private readonly IModelService modelService;
        private readonly IUserService userService;
        private readonly UserManager<User> userManager;
        private readonly IManufacturerService manufacturerService;

        public CarsController(ICarService carService, UserManager<User> userManager, IModelService modelService, IUserService userService, IManufacturerService manufacturerService)
        {
            this.carService = carService;
            this.userManager = userManager;
            this.modelService = modelService;
            this.userService = userService;
            this.manufacturerService = manufacturerService;
        }

        [HttpGet("Index")]
        [Authorize(Roles = "Admin, Customer")]
        public async Task<IActionResult> Index()
        {
            var carDTOs = await this.carService.GetAllAsync();

            var carsView = new List<CarViewModel>();
            var user = await userManager.FindByNameAsync(User.Identity.Name);

            if (User.IsInRole("Customer"))
            {
                foreach (var item in carDTOs.Where(c => c.UserId == user.Id))
                {
                    carsView.Add(new CarViewModel
                    {
                        CarId = item.Id,
                        CarIDNumber = item.VehicleIDNumber,
                        LicensePlate = item.LicensePlate,
                        OwnerName = item.OwnerName,
                        ManufacturerId = item.ManufacturerId,
                        ModelId = item.ModelId,
                        OwnerId = item.UserId,
                        YearOfManufacture = item.YearOfManufacture
                    });
                }

                return await Task.FromResult(View(carsView));
            }


            foreach (var item in carDTOs)
            {
                carsView.Add(new CarViewModel
                {
                    CarId = item.Id,
                    CarIDNumber = item.VehicleIDNumber,
                    LicensePlate = item.LicensePlate,
                    OwnerName = item.OwnerName,
                    ManufacturerId = item.ManufacturerId,
                    ModelId = item.ModelId,
                    OwnerId = item.UserId,
                    YearOfManufacture = item.YearOfManufacture
                });
            }
            return await Task.FromResult(View(carsView));
        }

        [HttpGet("Edit/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id)
        {
            var car = await this.carService.GetAsync(id);

            var carView = new CarCreateViewModel
            {
                CarId = car.Id,
                CarIDNumber = car.VehicleIDNumber,
                LicensePlate = car.LicensePlate,
                ModelId = car.ModelId,
                OwnerId = car.UserId,
                YearOfManufacture = car.YearOfManufacture,
            };

            return View(carView);

        }

        [HttpPost("Edit/{id}")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(CarCreateViewModel car)
        {

            if (ModelState.IsValid)
            {
                var carDTO = new CarDTO
                {
                    Id = car.CarId,
                    LicensePlate = car.LicensePlate,
                    ModelId = car.ModelId,
                    UserId = car.OwnerId,
                    VehicleIDNumber = car.CarIDNumber,
                    YearOfManufacture = car.YearOfManufacture
                };
                var updated = await carService.UpdateAsync(carDTO);
                return RedirectToAction(nameof(Index));
            }
            return View(car);
        }

        [HttpGet("Delete/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            var car = await carService.GetAsync(id);
            var carView = new CarViewModel
            {
                CarId = car.Id,
                CarIDNumber = car.VehicleIDNumber,
                LicensePlate = car.LicensePlate,
                OwnerName = car.OwnerName,
                ManufacturerId = car.ManufacturerId,
                ModelId = car.ModelId,
                OwnerId = car.UserId,
                YearOfManufacture = car.YearOfManufacture

            };

            return View(carView);
        }

        [HttpPost("Delete/{id}")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(CarViewModel car)
        {
            try
            {
                var result = await carService.DeleteAsync(car.CarId);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet("Create")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create()
        {
            var models = await this.modelService.GetAllAsync();
            var modelsView = new List<ModelViewModel>();

            foreach (var item in models)
            {
                modelsView.Add(new ModelViewModel
                {
                    ModelId = item.Id,
                    ManufacturerId = item.Id,
                    Name = item.Name
                });
            }

            ViewData["ModelId"] = new SelectList(modelsView, "ModelId", "Name");


            var users = await this.userService.GetAllAsync();
            var usersView = new List<UserViewModel>();

            foreach (var user in users)
            {
                usersView.Add(new UserViewModel
                {
                    UserId = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.Phone,
                    Email = user.Email
                });
            }

            ViewData["OwnerId"] = new SelectList(usersView, "UserId", "Email");
            return View();
        }

        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(CarCreateViewModel car)
        {
            if (ModelState.IsValid)
            {
                var carDTO = new CarDTO
                {
                    Id = car.CarId,
                    LicensePlate = car.LicensePlate,
                    ModelId = car.ModelId,
                    UserId = car.OwnerId,
                    VehicleIDNumber = car.CarIDNumber,
                    YearOfManufacture = car.YearOfManufacture
                };
                var created = await this.carService.CreateAsync(carDTO);
                return RedirectToAction(nameof(Index));
            }

            var models = await this.modelService.GetAllAsync();
            var modelsView = new List<ModelViewModel>();

            foreach (var item in models)
            {
                modelsView.Add(new ModelViewModel
                {
                    ManufacturerId = item.Id,
                    Name = item.Name
                });
            }

            ViewData["ModelId"] = new SelectList(modelsView, "ModelId", "Name", car.ModelId);


            var users = await this.userService.GetAllAsync();
            var usersView = new List<UserViewModel>();

            foreach (var user in users)
            {
                usersView.Add(new UserViewModel
                {
                    UserId = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.Phone,
                    Email = user.Email
                });
            }

            ViewData["OwnerId"] = new SelectList(usersView, "UserId", "Email", car.OwnerId);

            return View(car);
        }

        [HttpGet("Details/{id}")]
        [Authorize(Roles = "Admin, Customer")]
        public async Task<IActionResult> Details(int id)
        {
            var carDTO = await this.carService.GetAsync(id);

            var carDetailsView = new CarDetailsViewModel();

            var manufacturerName = this.manufacturerService.GetAsync(carDTO.ManufacturerId).Result.Name;

            var modelName = this.modelService.GetAsync(carDTO.ModelId).Result.Name;

            carDetailsView.CarId = carDTO.Id;
            carDetailsView.CarIDNumber = carDTO.VehicleIDNumber;
            carDetailsView.LicensePlate = carDTO.LicensePlate;
            carDetailsView.OwnerName = carDTO.OwnerName;
            carDetailsView.ManufacturerId = carDTO.ManufacturerId;
            carDetailsView.ManufacturerName = manufacturerName;
            carDetailsView.ModelId = carDTO.ModelId;
            carDetailsView.ModelName = modelName;
            carDetailsView.OwnerId = carDTO.UserId;
            carDetailsView.YearOfManufacture = carDTO.YearOfManufacture;
            carDetailsView.ServiceHistoryId = carDTO.ServiceHistoryId;

            return View(carDetailsView);
        }

        [HttpGet("UserCars/{id}")]
        [Authorize(Roles = "Admin, Customer")]
        public async Task<IActionResult> UserCars(int id)
        {
            var carDTOs = await this.carService.GetAllForCustomerAsync(id);
          
            var carsView = new List<CarViewModel>();
            var user = await userManager.FindByIdAsync(id.ToString());

            foreach (var item in carDTOs)
            {
                carsView.Add(new CarViewModel
                {
                    CarId = item.Id,
                    CarIDNumber = item.VehicleIDNumber,
                    LicensePlate = item.LicensePlate,
                    OwnerName = item.OwnerName,
                    ManufacturerId = item.ManufacturerId,
                    ModelId = item.ModelId,
                    OwnerId = item.UserId,
                    YearOfManufacture = item.YearOfManufacture
                });
            }
            return await Task.FromResult(View(carsView));
        }
    }
}
