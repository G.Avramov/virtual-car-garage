﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using VirtualCarGarage.MVC.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Models;
using Microsoft.AspNetCore.Authorization;

namespace VirtualCarGarage.MVC.Controllers
{
    [Route("[controller]")]
    [Authorize(Roles = "Admin")]
    public class ModelsController : Controller
    {
        private readonly IModelService modelService;
        private readonly IManufacturerService manufacturerService;

        public ModelsController(IModelService modelService, IManufacturerService manufacturerService)
        {
            this.modelService = modelService;
            this.manufacturerService = manufacturerService;
        }

        [HttpGet("Index")]
        public async Task<IActionResult> Index()
        {
            var modelsDTO = await this.modelService
               .GetAllAsync();
            var modelsView = new List<ModelViewModel>();

            foreach (var item in modelsDTO)
            {
                modelsView.Add(new ModelViewModel
                {
                    ModelId = item.Id,
                    Name = item.Name,
                    ManufacturerId = item.ManufacturerId,
                    ManufacturerName = item.ManufacturerName
                });
            }

            return View(modelsView);
        }

        [HttpGet("Edit/{id}")]
        public async Task<IActionResult> Edit(int id)
        {
            var model = await this.modelService.GetAsync(id);
            
            var modelView = new ModelCreateViewModel
            {
                ModelId = model.Id,
                Name= model.Name,
                ManufacturerId = model.ManufacturerId
            };

            var manufacturers = await this.manufacturerService.GetAllAsync();

            var manufacturersView = new List<ManufacturerViewModel>();

            foreach (var item in manufacturers)
            {
                manufacturersView.Add(new ManufacturerViewModel
                {
                    ManufacturerId = item.Id,
                    Name = item.Name
                });
            }

            ViewData["ManufacturerId"] = new SelectList(manufacturersView, "ManufacturerId", "Name", modelView.ManufacturerId);

            return View(modelView);

        }

        [HttpPost("Edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ModelCreateViewModel model)
        {

            if (ModelState.IsValid)
            {
                var modelDTO = new ModelDTO
                {
                    Id = model.ModelId,
                    Name = model.Name,
                    ManufacturerId = model.ManufacturerId
                };
                var updated = await modelService.UpdateAsync(modelDTO);
                return RedirectToAction(nameof(Index));
            }

            var manufacturers = await this.manufacturerService.GetAllAsync();

            var manufacturersView = new List<ManufacturerViewModel>();

            foreach (var item in manufacturers)
            {
                manufacturersView.Add(new ManufacturerViewModel
                {
                    ManufacturerId = item.Id,
                    Name = item.Name
                });
            }

            ViewData["ManufacturerId"] = new SelectList(manufacturersView, "ManufacturerId", "Name", model.ManufacturerId);

            return View(model);
        }

        [HttpGet("Create")]
        public async Task<IActionResult> Create()
        {
            var manufacturers = await this.manufacturerService.GetAllAsync();

            var manufacturersView = new List<ManufacturerViewModel>();

            foreach (var item in manufacturers)
            {
                manufacturersView.Add(new ManufacturerViewModel
                {
                    ManufacturerId = item.Id,
                    Name = item.Name
                });
            }

            ViewData["ManufacturerId"] = new SelectList(manufacturersView, "ManufacturerId", "Name");

            return View();
        }

        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,ManufacturerId")] ModelCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var modelDTO = new ModelDTO
                {
                    Id = model.ModelId,
                    Name = model.Name,
                    ManufacturerId = model.ManufacturerId
                };
                var created = await this.modelService.CreateAsync(modelDTO);
                return RedirectToAction(nameof(Index));
            }

            var manufacturers = await this.manufacturerService.GetAllAsync();

            var manufacturersView = new List<ManufacturerViewModel>();

            foreach (var item in manufacturers)
            {
                manufacturersView.Add(new ManufacturerViewModel
                {
                    ManufacturerId = item.Id,
                    Name = item.Name
                });
            }

            ViewData["ManufacturerId"] = new SelectList(manufacturersView, "ManufacturerId", "Name", model.ManufacturerId);

            return View(model);
        }

        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var model = await modelService.GetAsync(id);
            var modelView = new ModelViewModel
            {
                ModelId = model.Id,
                Name = model.Name,
                ManufacturerId = model.ManufacturerId
            };

            return View(modelView);
        }

        [HttpPost("Delete/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Delete(ModelViewModel model)
        {
            try
            {
                var result = await modelService.DeleteAsync(model.ModelId);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
