﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCarGarage.Data.Models;
using VirtualCarGarage.MVC.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.MVC.Controllers
{
    [Route("[controller]")]
    [Authorize]
    public class ServiceHistoriesController : Controller
    {
        private readonly IServiceHistoryService serviceHistoryService;
        private readonly UserManager<User> userManager;
        private readonly ICarService carService;

        public ServiceHistoriesController(IServiceHistoryService serviceHistoryService, UserManager<User> userManager, ICarService carService)
        {
            this.serviceHistoryService = serviceHistoryService;
            this.userManager = userManager;
            this.carService = carService;
        }

        [HttpGet("Index")]
        [Authorize(Roles = "Admin, Customer")]
        public async Task<IActionResult> Index()
        {

            if (User.IsInRole("Customer"))
            {
                var user = await this.userManager.GetUserAsync(HttpContext.User);

                var userServiceHistories = await this.serviceHistoryService.GetByUserIdAsync(user.Id);

                var userServiceHistoriesView = new List<ServiceHistoryViewModel>();

                foreach (var item in userServiceHistories)
                {
                    userServiceHistoriesView.Add(new ServiceHistoryViewModel
                    {
                        ServiceHistoryId = item.Id,
                        CarId = item.CarId
                    });
                }

                return View(userServiceHistoriesView);
            }

            var serviceHistories = await this.serviceHistoryService.GetAllAsync();

            var serviceHistoriesView = new List<ServiceHistoryViewModel>();

            foreach (var item in serviceHistories)
            {
                serviceHistoriesView.Add(new ServiceHistoryViewModel
                {
                    ServiceHistoryId = item.Id,
                    CarId = item.CarId
                });
            }

            return View(serviceHistoriesView);
        }

        [HttpGet("Create")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create()
        {
            var cars = await this.carService.GetAllWithoutServiceHistoryAsync();

            var carsView = new List<CarViewModel>();

            foreach (var car in cars)
            {
                carsView.Add(new CarViewModel
                {
                    CarId = car.Id,
                    CarIDNumber = car.VehicleIDNumber,
                    LicensePlate = car.LicensePlate,
                    OwnerName = car.OwnerName,
                    ManufacturerId = car.ManufacturerId,
                    ModelId = car.ModelId,
                    OwnerId = car.UserId,
                    YearOfManufacture = car.YearOfManufacture
                });
            }

            ViewData["CarId"] = new SelectList(carsView, "CarId", "LicensePlate");

            return View();
        }

        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(ServiceHistoryCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var serviceHistoryDTO = new ServiceHistoryDTO
                {
                    CarId = model.CarId
                };
                var created = await this.serviceHistoryService.CreateAsync(serviceHistoryDTO);
                return RedirectToAction(nameof(Index));
            }

            var cars = await this.carService.GetAllWithoutServiceHistoryAsync();

            var carsView = new List<CarViewModel>();

            foreach (var car in cars)
            {
                carsView.Add(new CarViewModel
                {
                    CarId = car.Id,
                    CarIDNumber = car.VehicleIDNumber,
                    LicensePlate = car.LicensePlate,
                    OwnerName = car.OwnerName,
                    ManufacturerId = car.ManufacturerId,
                    ModelId = car.ModelId,
                    OwnerId = car.UserId,
                    YearOfManufacture = car.YearOfManufacture
                });
            }

            ViewData["CarId"] = new SelectList(carsView, "CarId", "LicensePlate", model.CarId);

            return View(model);
        }

        [HttpGet("Edit/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id)
        {
            var serviceHistory = await this.serviceHistoryService.GetAsync(id);

            var serviceHistoryView = new ServiceHistoryViewModel
            {
                ServiceHistoryId = serviceHistory.Id,
                CarId = serviceHistory.CarId
            };

            var cars = await this.carService.GetAllWithoutServiceHistoryAsync();

            var carsView = new List<CarViewModel>();

            foreach (var car in cars)
            {
                carsView.Add(new CarViewModel
                {
                    CarId = car.Id,
                    CarIDNumber = car.VehicleIDNumber,
                    LicensePlate = car.LicensePlate,
                    OwnerName = car.OwnerName,
                    ManufacturerId = car.ManufacturerId,
                    ModelId = car.ModelId,
                    OwnerId = car.UserId,
                    YearOfManufacture = car.YearOfManufacture
                });
            }

            ViewData["CarId"] = new SelectList(carsView, "CarId", "LicensePlate");

            return View(serviceHistoryView);

        }

        [HttpPost("Edit/{id}")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(ServiceHistoryViewModel serviceHistory)
        {

            if (ModelState.IsValid)
            {
                var serviceHistoryDTO = new ServiceHistoryDTO
                {
                    Id = serviceHistory.ServiceHistoryId,
                    CarId = serviceHistory.CarId
                };
                var updated = await serviceHistoryService.UpdateAsync(serviceHistoryDTO);
                return RedirectToAction(nameof(Index));
            }

            var cars = await this.carService.GetAllWithoutServiceHistoryAsync();

            var carsView = new List<CarViewModel>();

            foreach (var car in cars)
            {
                carsView.Add(new CarViewModel
                {
                    CarId = car.Id,
                    CarIDNumber = car.VehicleIDNumber,
                    LicensePlate = car.LicensePlate,
                    OwnerName = car.OwnerName,
                    ManufacturerId = car.ManufacturerId,
                    ModelId = car.ModelId,
                    OwnerId = car.UserId,
                    YearOfManufacture = car.YearOfManufacture
                });
            }

            ViewData["CarId"] = new SelectList(carsView, "CarId", "LicensePlate", serviceHistory.CarId);
            return View(serviceHistory);
        }

        [HttpGet("Delete/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            var serviceHistory = await serviceHistoryService.GetAsync(id);
            var serviceHistoryView = new ServiceHistoryViewModel
            {
                ServiceHistoryId = serviceHistory.Id,
                CarId = serviceHistory.CarId
            };

            return View(serviceHistoryView);
        }

        [HttpPost("Delete/{id}")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(ServiceHistoryViewModel serviceHistory)
        {
            try
            {
                var result = await serviceHistoryService.DeleteAsync(serviceHistory.ServiceHistoryId);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet("Details/{id}")]
        [Authorize(Roles = "Admin, Customer")]
        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var serviceHistory = await this.serviceHistoryService.GetAsync(id);

                var ordersView = new List<OrderViewModel>();
                foreach (var item in serviceHistory.Orders)
                {
                    var servicesView = new List<ServiceViewModel>();
                    foreach (var s in item.Services)
                    {
                        servicesView.Add(new ServiceViewModel
                        {
                            ServiceId = s.Id,
                            Name = s.Name,
                            Price = s.Price
                        });
                    }
                    ordersView.Add(new OrderViewModel
                    {
                        OrderId = item.Id,
                        OrderDate = item.OrderDate,
                        ServiceHistoryId = item.ServiceHistoryId,
                        ServicesOrdered = servicesView
                    });
                }
                var serviceHistoryView = new ServiceHistoryViewModel()
                {
                    ServiceHistoryId = serviceHistory.Id,
                    CarId = serviceHistory.CarId,
                    Orders = ordersView
                };

                return View(serviceHistoryView);
            }
            catch (Exception)
            {

                return View();

            }

        }
    }
}
