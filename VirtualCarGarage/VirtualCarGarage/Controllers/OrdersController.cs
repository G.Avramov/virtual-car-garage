﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VirtualCarGarage.Data.Models;
using VirtualCarGarage.MVC.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.MVC.Controllers
{
    [Route("[controller]")]
    [Authorize]
    public class OrdersController : Controller
    {
        private readonly IOrderService orderService;
        private readonly UserManager<User> userManager;
        private readonly IServiceHistoryService serviceHistoryService;
        private readonly IServiceService serviceService;

        public OrdersController(IOrderService orderService, UserManager<User> userManager, IServiceHistoryService serviceHistoryService, IServiceService serviceService)
        {
            this.orderService = orderService;
            this.userManager = userManager;
            this.serviceHistoryService = serviceHistoryService;
            this.serviceService = serviceService;
        }

        [HttpGet("Index")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            if (User.IsInRole("Admin"))
            {
                var orders = await this.orderService.GetAllAsync();

                var ordersView = new List<OrderViewModel>();

                foreach (var item in orders)
                {
                    var services = new List<ServiceViewModel>();
                    foreach (var service in item.Services)
                    {
                        services.Add(new ServiceViewModel
                        {
                            ServiceId = service.Id,
                            Name = service.Name,
                            Price = service.Price
                        });
                    }
                    ordersView.Add(new OrderViewModel
                    {
                        OrderId = item.Id,
                        OrderDate = item.OrderDate,
                        ServiceHistoryId = item.ServiceHistoryId,
                        ServicesOrdered = services
                    });
                }

                return await Task.FromResult(View(ordersView));                
            }
            
            return View();
        }

        [HttpGet("Create")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create()
        {
            var serviceHistories = await this.serviceHistoryService.GetAllAsync();

            var serviceHistoriesView = new List<ServiceHistoryViewModel>();

            foreach (var item in serviceHistories)
            {
                serviceHistoriesView.Add(new ServiceHistoryViewModel
                {
                   ServiceHistoryId = item.Id,
                   CarId = item.CarId                  
                });
            }

            ViewData["ServiceHistoryId"] = new SelectList(serviceHistoriesView, "ServiceHistoryId", "ServiceHistoryId");

            return View();
        }

        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(OrderCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                var orderDTO = new OrderDTO
                {
                    ServiceHistoryId = model.ServiceHistoryId,
                    OrderDate = model.OrderDate
                };
                var created = await this.orderService.CreateAsync(orderDTO);
                return RedirectToAction(nameof(Index));
            }

            var serviceHistories = await this.serviceHistoryService.GetAllAsync();

            var serviceHistoriesView = new List<ServiceHistoryViewModel>();

            foreach (var item in serviceHistories)
            {
                serviceHistoriesView.Add(new ServiceHistoryViewModel
                {
                    ServiceHistoryId = item.Id,
                    CarId = item.CarId
                });
            }

            ViewData["ServiceHistoryId"] = new SelectList(serviceHistoriesView, "ServiceHistoryId", "ServiceHistoryId", model.ServiceHistoryId);

            return View(model);
        }

        [HttpGet("AddServices/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddServices(int id)
        {
            try
            {
                var order = await this.orderService.GetAsync(id);
            
               /* var orderView = new OrderViewModel()
                {
                    OrderId = order.Id,
                    ServiceHistoryId = order.ServiceHistoryId,
                    OrderDate = order.OrderDate,
                    ServicesOrdered = servicesView
                };*/

                var allServices = await this.serviceService.GetAllAsync();
                var allServicesView = new List<ServiceViewModel>();

                foreach (var item in allServices)
                {
                    allServicesView.Add(new ServiceViewModel
                    {
                        ServiceId = item.Id,
                        Name = item.Name,
                        Price = item.Price
                    });
                }

                var result = new OrderAddServicesViewModel()
                {
                    OrderId = order.Id,
                    Services = allServicesView
                };
               // ViewData["ServiceId"] = new SelectList(allServicesView, "ServiceId", "Name");
                return View(result);
            }
            catch
            {
                return NotFound();
            }
           
        }

        [HttpPost("AddServices/{id}")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddServices(OrderAddServicesViewModel model, List<ServiceViewModel> serviceViewModels)
        {
            if (ModelState.IsValid)
            {               
                try                {
                    var services = new List<ServiceDTO>();
                    foreach (var item in serviceViewModels)
                    {
                        services.Add(new ServiceDTO
                        {
                            Id = item.ServiceId,
                            Name = item.Name,
                            Price = item.Price
                        });
                    }
                    await this.orderService.AddServices(model.OrderId, services);
                }
                catch
                {
                    return NotFound();
                }

                return RedirectToAction(nameof(Index));
            }

            return View(model);
        }

        [HttpGet("Delete/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int id)
        {
            var order = await orderService.GetAsync(id);
            var orderView = new OrderViewModel
            {
                OrderId = order.Id,
                OrderDate = order.OrderDate,
                ServiceHistoryId = order.ServiceHistoryId
            };

            return View(orderView);
        }

        [HttpPost("Delete/{id}")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(OrderViewModel order)
        {
            try
            {
                var result = await orderService.DeleteAsync(order.OrderId);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet("Edit/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id)
        {
            var order = await this.orderService.GetAsync(id);

            var orderView = new OrderViewModel
            {
                OrderId = order.Id,
                OrderDate = order.OrderDate,
                ServiceHistoryId = order.ServiceHistoryId
            };

            var serviceHistories = await this.serviceHistoryService.GetAllAsync();

            var serviceHistoriesView = new List<ServiceHistoryViewModel>();

            foreach (var item in serviceHistories)
            {
                serviceHistoriesView.Add(new ServiceHistoryViewModel
                {
                    ServiceHistoryId = item.Id,
                    CarId = item.CarId
                });
            }

            ViewData["ServiceHistoryId"] = new SelectList(serviceHistoriesView, "ServiceHistoryId", "ServiceHistoryId");            

            return View(orderView);

        }

        [HttpPost("Edit/{id}")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(OrderViewModel order)
        {

            if (ModelState.IsValid)
            {
                var orderDTO = new OrderEditDTO
                {
                    Id = order.OrderId,
                    OrderDate = order.OrderDate,
                    ServiceHistoryId = order.ServiceHistoryId
                };
                var updated = await orderService.UpdateAsync(orderDTO);
                return RedirectToAction(nameof(Index));
            }

            var serviceHistories = await this.serviceHistoryService.GetAllAsync();

            var serviceHistoriesView = new List<ServiceHistoryViewModel>();

            foreach (var item in serviceHistories)
            {
                serviceHistoriesView.Add(new ServiceHistoryViewModel
                {
                    ServiceHistoryId = item.Id,
                    CarId = item.CarId
                });
            }

            ViewData["ServiceHistoryId"] = new SelectList(serviceHistoriesView, "ServiceHistoryId", "ServiceHistoryId", order.ServiceHistoryId);

            return View(order);
        }


        [HttpGet("Details/{id}")]
        [Authorize(Roles = "Admin, Customer")]
        public async Task<IActionResult> Details(int id)
        {
            var order = await this.orderService.GetAsync(id);

            var orderView = new OrderViewModel();

            var servicesView = new List<ServiceViewModel>();
            foreach (var s in order.Services)
            {
                servicesView.Add(new ServiceViewModel
                {
                    ServiceId = s.Id,
                    Name = s.Name,
                    Price = s.Price
                });
            }
            orderView.OrderId = id;
            orderView.OrderDate = order.OrderDate;
            orderView.ServiceHistoryId = order.ServiceHistoryId;
            orderView.ServicesOrdered = servicesView;

            return View(orderView);
        }
    }
}
