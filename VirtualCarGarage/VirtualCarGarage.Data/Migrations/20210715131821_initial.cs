﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace VirtualCarGarage.Data.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 20, nullable: false),
                    LastName = table.Column<string>(maxLength: 20, nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Manufacturers",
                columns: table => new
                {
                    ManufacturerId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Manufacturers", x => x.ManufacturerId);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Models",
                columns: table => new
                {
                    ModelId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    ManufacturerId = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Models", x => x.ModelId);
                    table.ForeignKey(
                        name: "FK_Models_Manufacturers_ManufacturerId",
                        column: x => x.ManufacturerId,
                        principalTable: "Manufacturers",
                        principalColumn: "ManufacturerId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Cars",
                columns: table => new
                {
                    CarId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ModelId = table.Column<int>(nullable: false),
                    LicensePlate = table.Column<string>(maxLength: 10, nullable: false),
                    YearOfManufacture = table.Column<int>(nullable: false),
                    VehicleIDNumber = table.Column<string>(maxLength: 17, nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cars", x => x.CarId);
                    table.ForeignKey(
                        name: "FK_Cars_Models_ModelId",
                        column: x => x.ModelId,
                        principalTable: "Models",
                        principalColumn: "ModelId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Cars_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ServiceHistories",
                columns: table => new
                {
                    ServiceHistoryId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CarId = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceHistories", x => x.ServiceHistoryId);
                    table.ForeignKey(
                        name: "FK_ServiceHistories_Cars_CarId",
                        column: x => x.CarId,
                        principalTable: "Cars",
                        principalColumn: "CarId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    OrderId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OrderDate = table.Column<DateTime>(type: "date", nullable: false),
                    ServiceHistoryId = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.OrderId);
                    table.ForeignKey(
                        name: "FK_Orders_ServiceHistories_ServiceHistoryId",
                        column: x => x.ServiceHistoryId,
                        principalTable: "ServiceHistories",
                        principalColumn: "ServiceHistoryId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Services",
                columns: table => new
                {
                    ServiceId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Price = table.Column<decimal>(type: "decimal(5,2)", nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    OrderId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => x.ServiceId);
                    table.ForeignKey(
                        name: "FK_Services_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "OrderId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 2, "5c5d6b83-4a53-4f14-92ee-2f48072abcbd", "Customer", "CUSTOMER" },
                    { 1, "6d9dab25-8d9d-467c-9ded-020c8c28855e", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "IsDeleted", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[,]
                {
                    { 2, 0, "2de5c24a-88eb-487f-afcd-be8e251b6c5e", "customer@customer.com", false, "Tosho", false, "Toshev", false, null, "CUSTOMER@CUSTOMER.COM", "CUSTOMER@CUSTOMER.COM", "AQAAAAEAACcQAAAAEMK+w15O4pjC/CCtPQVNOIvOfRyOEDjReRejmXMb0go2fd+SUmB5kZj11w2CauyNmA==", "5555599999", false, "d5ea30ac-0221-42f8-9c47-c89578dd6bb8", false, "customer@customer.com" },
                    { 1, 0, "a3f15811-d1a8-4a60-b786-1887ed9699eb", "admin@admin.com", false, "Ivan", false, "Ivanov", false, null, "ADMIN@ADMIN.COM", "ADMIN@ADMIN.COM", "AQAAAAEAACcQAAAAEI5wraKbzB8fKfQ+WoeSfGCOdMAHkMZ6xGtu7/7JhGFj7yug3r+eD7KuBzQf8SBWnQ==", "1234567890", false, "a9fb02ce-1a2d-4f3f-9c38-5cc264366788", false, "admin@admin.com" }
                });

            migrationBuilder.InsertData(
                table: "Manufacturers",
                columns: new[] { "ManufacturerId", "IsDeleted", "Name" },
                values: new object[,]
                {
                    { 15, false, "Nissan" },
                    { 14, false, "Audi" },
                    { 13, false, "Mazda" },
                    { 12, false, "Hyundai" },
                    { 11, false, "Alfa Romeo" },
                    { 1, false, "Ford" },
                    { 9, false, "Peugeot" },
                    { 2, false, "Toyota" },
                    { 3, false, "Kia" },
                    { 4, false, "Infiniti" },
                    { 10, false, "BMW" },
                    { 6, false, "Mitsubishi" },
                    { 7, false, "Lexus" },
                    { 8, false, "Porsche" },
                    { 5, false, "Volkswagen" }
                });

            migrationBuilder.InsertData(
                table: "Services",
                columns: new[] { "ServiceId", "IsDeleted", "Name", "OrderId", "Price" },
                values: new object[,]
                {
                    { 71, false, "Stabilizer Bar Links Replacement", null, 148.96m },
                    { 64, false, "Crankshaft Position Sensor Replacement", null, 269.65m },
                    { 70, false, "Power Steering Input Shaft Seal Replacement", null, 102.78m },
                    { 69, false, "Lubricate Steering and Suspension", null, 105.58m },
                    { 68, false, "Ball Joint Replacement (Rear)", null, 179.26m },
                    { 67, false, "Ball Joint Replacement (Front)", null, 265.46m },
                    { 66, false, "Air Bag Spring Replacement", null, 262.40m },
                    { 65, false, "Air Shock Replacement", null, 193.38m },
                    { 63, false, "Speedometer Sensor Replacement", null, 58.06m },
                    { 58, false, "Car Door Mirror Replacement", null, 124.34m },
                    { 61, false, "Mass Airflow Sensor Replacement", null, 70.01m },
                    { 60, false, "Air Charge Temperature Sensor Replacement", null, 271.56m },
                    { 59, false, "ABS Speed Sensor Replacement", null, 199.95m },
                    { 72, false, "Car AC Control Switch Replacement", null, 276.32m },
                    { 57, false, "Trunk Light Bulb Replacement", null, 153.37m },
                    { 56, false, "Headlight Bulb Replacement", null, 260.38m },
                    { 55, false, "Brake Light Bulb Replacement", null, 156.45m },
                    { 54, false, "Ignition Coil Replacement", null, 265.39m },
                    { 53, false, "Ignition Relay Replacement", null, 233.48m },
                    { 62, false, "Oil Temperature Sensor Replacement", null, 205.50m },
                    { 73, false, "Blower Motor Switch Replacement", null, 96.52m },
                    { 77, false, "Ignition Switch Replacement", null, 146.77m },
                    { 75, false, "Clutch Switch Replacement", null, 140.19m },
                    { 97, false, "Wiper Gearbox Replacement", null, 190.23m },
                    { 96, false, "Windshield Wiper/Washer System Inspection", null, 218.12m },
                    { 95, false, "Windshield Wiper Switch Replacement", null, 174.12m },
                    { 94, false, "Windshield Wiper Refill Replacement", null, 291.92m },
                    { 93, false, "Windshield Wiper Motor Replacement", null, 67.28m },
                    { 92, false, "Windshield Wiper Linkage Replacement", null, 92.41m },
                    { 91, false, "Windshield Wiper Blade Replacement", null, 58.05m },
                    { 90, false, "Windshield Wiper Arm Replacement", null, 140.30m },
                    { 89, false, "Windshield Washer Pump Replacement", null, 112.94m },
                    { 88, false, "Window Motor Regulator Replacement", null, 94.00m },
                    { 74, false, "Clutch Safety Switch Replacement", null, 234.83m },
                    { 87, false, "Washer fluid does not spray onto windshield Inspection", null, 81.16m },
                    { 85, false, "Clean Windshield Washer Tubes and Jets Inspection", null, 117.47m },
                    { 84, false, "Adjust Windshield Washer Jets", null, 85.50m },
                    { 83, false, "Window Motor Regulator Replacement", null, 221.61m },
                    { 82, false, "Power Window Switch Repair", null, 201.88m },
                    { 81, false, "Spare Tire Installation", null, 116.69m },
                    { 80, false, "Tire Rotation", null, 291.07m },
                    { 79, false, "Turn Signal Switch Replacement", null, 192.61m },
                    { 78, false, "Power Window Switch Repair", null, 295.74m },
                    { 52, false, "Ballast Resistor Replacement", null, 250.02m },
                    { 76, false, "Car Door Lock Switch Replacement", null, 164.44m },
                    { 86, false, "Clean and Repack Wheel Bearing", null, 230.68m },
                    { 51, false, "Brake Hose Replacement", null, 77.35m },
                    { 46, false, "Car AC Accumulator Replacement", null, 201.78m },
                    { 49, false, "Car AC Low Pressure Hose Replacement", null, 292.49m },
                    { 21, false, "Brake Pedal vibrates or shakes Inspection", null, 203.80m },
                    { 20, false, "Battery Light is on Inspection", null, 157.10m },
                    { 19, false, "Not able to change gears Inspection", null, 86.68m },
                    { 18, false, "Speed Sensor Replacement", null, 236.44m },
                    { 17, false, "Transmission Oil Pressure Switch Replacement", null, 292.05m },
                    { 16, false, "Speedometer Cable Repair", null, 282.40m },
                    { 15, false, "Clutch Switch Replacement", null, 117.05m },
                    { 14, false, "Pre-purchase Car Inspection", null, 225.30m },
                    { 13, false, "Parking Brake Release Cable Replacement", null, 259.42m },
                    { 12, false, "Vacuum Pump Repair", null, 125.15m },
                    { 11, false, "Brake Drum Replacement", null, 152.03m },
                    { 10, false, "Anti-Lock Control Relay Replacement", null, 262.37m },
                    { 9, false, "Brake Pad Replacement", null, 140.05m },
                    { 8, false, "ABS Speed Sensor Replacement", null, 143.03m },
                    { 7, false, "Supercharger Belt Replacement", null, 241.10m },
                    { 6, false, "Timing Belt Replacement", null, 164.46m },
                    { 5, false, "Power Steering Belt Replacement", null, 290.66m },
                    { 4, false, "Car AC Belt Replacement", null, 209.54m },
                    { 3, false, "Car Battery Cable Replacement", null, 151.20m },
                    { 2, false, "Car Battery Replacement", null, 103.76m },
                    { 1, false, "Auxiliary Battery Replacement", null, 171.98m },
                    { 22, false, "Brake safety Inspection", null, 262.02m },
                    { 50, false, "Radiator Hose Repair", null, 247.17m },
                    { 23, false, "Loss of power Inspection", null, 71.24m },
                    { 25, false, "Interior Car Door Handle Replacement", null, 181.08m },
                    { 48, false, "Car AC Repair", null, 240.95m },
                    { 47, false, "Condenser Fan Relay Replacement", null, 115.10m },
                    { 45, false, "Fuel Filter Replacement", null, 63.49m },
                    { 44, false, "Gas Cap Replacement", null, 134.68m },
                    { 43, false, "Carburetor Repair", null, 195.79m },
                    { 42, false, "Oil Change", null, 261.36m },
                    { 40, false, "Cooling System Flush", null, 259.55m },
                    { 39, false, "Brake System Flush", null, 91.89m },
                    { 38, false, "Fuel Filter Replacement", null, 154.15m },
                    { 37, false, "Car Air Filter Replacement", null, 59.42m },
                    { 36, false, "Cabin Air Filter Replacement", null, 246.48m },
                    { 35, false, "Car AC Air Filter Replacement", null, 156.70m },
                    { 34, false, "Leak Detection Pump Replacement", null, 110.90m },
                    { 33, false, "Catalytic Converter Replacement", null, 194.91m },
                    { 32, false, "Canister Purge Solenoid Replacement", null, 118.69m },
                    { 31, false, "Valve Cover Replacement", null, 61.60m },
                    { 30, false, "Water Pump Replacement", null, 61.49m },
                    { 29, false, "Oil Temperature Sensor Replacement", null, 273.98m },
                    { 28, false, "Throttle Cable Repair", null, 239.69m },
                    { 27, false, "Trunk Lock Actuator Replacement", null, 281.17m },
                    { 26, false, "Trunk Latch Release Cable Replacement", null, 136.81m },
                    { 24, false, "Car Door Latch Replacement", null, 178.70m },
                    { 41, false, "Radiator Flush", null, 98.58m }
                });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "UserId", "RoleId" },
                values: new object[,]
                {
                    { 2, 2 },
                    { 1, 1 }
                });

            migrationBuilder.InsertData(
                table: "Models",
                columns: new[] { "ModelId", "IsDeleted", "ManufacturerId", "Name" },
                values: new object[,]
                {
                    { 435, false, 15, "Vanette Cargo" },
                    { 298, false, 12, "Getz" },
                    { 297, false, 12, "Genesis" },
                    { 296, false, 12, "Galloper" },
                    { 295, false, 12, "Elantra" },
                    { 294, false, 12, "Coupe" },
                    { 293, false, 12, "Atos Prime" },
                    { 292, false, 12, "Atos" },
                    { 291, false, 12, "Accent" },
                    { 290, false, 11, "XC90" },
                    { 289, false, 11, "XC70" },
                    { 288, false, 11, "XC60" },
                    { 287, false, 11, "V90" },
                    { 286, false, 11, "V70" },
                    { 285, false, 11, "V60" },
                    { 284, false, 11, "V50" },
                    { 283, false, 11, "V40" },
                    { 282, false, 11, "S90" },
                    { 281, false, 11, "S80" },
                    { 280, false, 11, "S70" },
                    { 279, false, 11, "S60" },
                    { 278, false, 11, "S40" },
                    { 277, false, 11, "C70 Coupe" },
                    { 276, false, 11, "C70 Cabrio" },
                    { 299, false, 12, "Grandeur" },
                    { 275, false, 11, "C70" },
                    { 300, false, 12, "H 350" },
                    { 302, false, 12, "H1 Bus" },
                    { 325, false, 13, "323" },
                    { 324, false, 13, "3" },
                    { 323, false, 13, "2" },
                    { 322, false, 13, "121" },
                    { 321, false, 12, "Veloster" },
                    { 320, false, 12, "Tucson" },
                    { 319, false, 12, "Trajet" },
                    { 318, false, 12, "Terracan" },
                    { 317, false, 12, "Sonata" },
                    { 316, false, 12, "Santa Fe" },
                    { 315, false, 12, "Matrix" },
                    { 314, false, 12, "Lantra" },
                    { 313, false, 12, "ix55" },
                    { 312, false, 12, "ix35" },
                    { 311, false, 12, "ix20" },
                    { 310, false, 12, "i40 CW" },
                    { 309, false, 12, "i40" },
                    { 308, false, 12, "i30 CW" },
                    { 307, false, 12, "i30" },
                    { 306, false, 12, "i20" },
                    { 305, false, 12, "i10" },
                    { 304, false, 12, "H200" },
                    { 303, false, 12, "H1 Van" },
                    { 301, false, 12, "H1" },
                    { 274, false, 11, "C30" },
                    { 273, false, 11, "850 kombi" },
                    { 272, false, 11, "850" },
                    { 243, false, 9, "605" },
                    { 242, false, 9, "508 SW" },
                    { 241, false, 9, "508" },
                    { 240, false, 9, "5008" },
                    { 239, false, 9, "407 SW" },
                    { 238, false, 9, "407" },
                    { 237, false, 9, "406" },
                    { 236, false, 9, "405" },
                    { 235, false, 9, "4008" },
                    { 234, false, 9, "4007" },
                    { 233, false, 9, "309" },
                    { 232, false, 9, "308 SW" },
                    { 231, false, 9, "308 CC" },
                    { 230, false, 9, "308" },
                    { 229, false, 9, "307 SW" },
                    { 228, false, 9, "307 CC" },
                    { 227, false, 9, "307" },
                    { 226, false, 9, "306" },
                    { 225, false, 9, "207 SW" },
                    { 224, false, 9, "207 CC" },
                    { 223, false, 9, "207" },
                    { 222, false, 9, "206 SW" },
                    { 221, false, 9, "206 CC" },
                    { 244, false, 9, "806" },
                    { 245, false, 9, "607" },
                    { 246, false, 9, "807" },
                    { 247, false, 9, "Bipper" },
                    { 271, false, 11, "460" },
                    { 270, false, 11, "360" },
                    { 269, false, 11, "340" },
                    { 268, false, 11, "240" },
                    { 267, false, 10, "Giulia" },
                    { 266, false, 10, "Giulietta" },
                    { 265, false, 10, "GT" },
                    { 264, false, 10, "Spider" },
                    { 263, false, 10, "Crosswagon" },
                    { 262, false, 10, "MiTo" },
                    { 261, false, 10, "GTV" },
                    { 326, false, 13, "323 Combi" },
                    { 260, false, 10, "Brera" },
                    { 258, false, 10, "166" },
                    { 257, false, 10, "164" },
                    { 256, false, 10, "159 Sportwagon" },
                    { 255, false, 10, "159" },
                    { 254, false, 10, "156 Sportwagon" },
                    { 253, false, 10, "156" },
                    { 252, false, 10, "155" },
                    { 251, false, 10, "147" },
                    { 250, false, 10, "146" },
                    { 249, false, 10, "145" },
                    { 248, false, 9, "RCZ" },
                    { 259, false, 10, "4C" },
                    { 436, false, 15, "X-Trail" },
                    { 327, false, 13, "323 Coupe" },
                    { 329, false, 13, "5" },
                    { 407, false, 15, "Juke" },
                    { 406, false, 15, "Insterstar" },
                    { 405, false, 15, "GT-R" },
                    { 404, false, 15, "e-NV200" },
                    { 403, false, 15, "Cabstar TL2 Valnik" },
                    { 402, false, 15, "Cabstar E - T" },
                    { 401, false, 15, "Almera Tino" },
                    { 400, false, 15, "Almera" },
                    { 399, false, 15, "370 Z" },
                    { 398, false, 15, "350 Z Roadster" },
                    { 397, false, 15, "350 Z" },
                    { 396, false, 15, "200 SX" },
                    { 395, false, 15, "100 NX" },
                    { 394, false, 14, "TTS" },
                    { 393, false, 14, "TT Roadster" },
                    { 392, false, 14, "TT Coupe" },
                    { 391, false, 14, "SQ5" },
                    { 390, false, 14, "S8" },
                    { 389, false, 14, "S7" },
                    { 388, false, 14, "S6/RS6" },
                    { 387, false, 14, "S5/S5 Cabriolet" },
                    { 386, false, 14, "S4/S4 Avant" },
                    { 385, false, 14, "S4 Cabriolet" },
                    { 408, false, 15, "King Cab" },
                    { 384, false, 14, "S3/S3 Sportback" },
                    { 409, false, 15, "Leaf" },
                    { 411, false, 15, "Maxima QX" },
                    { 434, false, 15, "Trade" },
                    { 433, false, 15, "Tiida" },
                    { 432, false, 15, "Terrano" },
                    { 431, false, 15, "Sunny" },
                    { 430, false, 15, "Serena" },
                    { 429, false, 15, "Qashqai" },
                    { 428, false, 15, "Pulsar" },
                    { 427, false, 15, "Primera Combi" },
                    { 426, false, 15, "Primera" },
                    { 425, false, 15, "Primastar Combi" },
                    { 424, false, 15, "Primastar" },
                    { 423, false, 15, "Pixo" },
                    { 422, false, 15, "Pickup" },
                    { 421, false, 15, "Patrol GR" },
                    { 420, false, 15, "Patrol" },
                    { 419, false, 15, "Pathfinder" },
                    { 418, false, 15, "NV400" },
                    { 417, false, 15, "NV200" },
                    { 416, false, 15, "NP300 Pickup" },
                    { 415, false, 15, "Note" },
                    { 414, false, 15, "Navara" },
                    { 413, false, 15, "Murano" },
                    { 412, false, 15, "Micra" },
                    { 410, false, 15, "Maxima" },
                    { 383, false, 14, "RS7" },
                    { 382, false, 14, "RS6 Avant" },
                    { 381, false, 14, "RS5" },
                    { 352, false, 14, "80" },
                    { 351, false, 14, "100 Avant" },
                    { 350, false, 14, "100" },
                    { 349, false, 13, "Xedox 6" },
                    { 348, false, 13, "RX-8" },
                    { 347, false, 13, "RX-7" },
                    { 346, false, 13, "Premacy" },
                    { 345, false, 13, "MX-6" },
                    { 344, false, 13, "MX-5" },
                    { 343, false, 13, "MX-3" },
                    { 342, false, 13, "MPV" },
                    { 341, false, 13, "Demio" },
                    { 340, false, 13, "CX-9" },
                    { 339, false, 13, "CX-7" },
                    { 338, false, 13, "CX-5" },
                    { 337, false, 13, "CX-3" },
                    { 336, false, 13, "BT" },
                    { 335, false, 13, "B2500" },
                    { 334, false, 13, "B-Fighter" },
                    { 333, false, 13, "626 Combi" },
                    { 332, false, 13, "626" },
                    { 331, false, 13, "6 Combi" },
                    { 330, false, 13, "6" },
                    { 353, false, 14, "80 Avant" },
                    { 354, false, 14, "80 Cabrio" },
                    { 355, false, 14, "90" },
                    { 356, false, 14, "A1" },
                    { 380, false, 14, "RS4/RS4 Avant" },
                    { 379, false, 14, "RS4 Cabriolet" },
                    { 378, false, 14, "R8" },
                    { 377, false, 14, "Q7" },
                    { 376, false, 14, "Q5" },
                    { 375, false, 14, "Q3" },
                    { 374, false, 14, "A8 Long" },
                    { 373, false, 14, "A8" },
                    { 372, false, 14, "A7" },
                    { 371, false, 14, "A6 Avant" },
                    { 370, false, 14, "A6 Allroad" },
                    { 220, false, 9, "206" },
                    { 369, false, 14, "A6" },
                    { 367, false, 14, "A5 Cabriolet" },
                    { 366, false, 14, "A5" },
                    { 365, false, 14, "A4 Cabriolet" },
                    { 364, false, 14, "A4 Avant" },
                    { 363, false, 14, "A4 Allroad" },
                    { 362, false, 14, "A4" },
                    { 361, false, 14, "A3 Sportback" },
                    { 360, false, 14, "A3 Limuzina" },
                    { 359, false, 14, "A3 Cabriolet" },
                    { 358, false, 14, "A3" },
                    { 357, false, 14, "A2" },
                    { 368, false, 14, "A5 Sportback" },
                    { 328, false, 13, "323 F" },
                    { 219, false, 9, "205 Cabrio" },
                    { 217, false, 9, "2008" },
                    { 79, false, 2, "Yaris Verso" },
                    { 78, false, 2, "Yaris" },
                    { 77, false, 2, "Verso" },
                    { 76, false, 2, "Urban Cruiser" },
                    { 75, false, 2, "Tundra" },
                    { 74, false, 2, "Supra" },
                    { 73, false, 2, "Starlet" },
                    { 72, false, 2, "Sequoia" },
                    { 71, false, 2, "RAV4" },
                    { 70, false, 2, "Prius" },
                    { 69, false, 2, "Picnic" },
                    { 68, false, 2, "Paseo" },
                    { 67, false, 2, "MR2" },
                    { 66, false, 2, "Land Cruiser" },
                    { 65, false, 2, "Hilux" },
                    { 64, false, 2, "Highlander" },
                    { 63, false, 2, "Hiace Van" },
                    { 62, false, 2, "Hiace" },
                    { 61, false, 2, "GT86" },
                    { 60, false, 2, "FJ Cruiser" },
                    { 59, false, 2, "Corolla Verso" },
                    { 58, false, 2, "Corolla sedan" },
                    { 57, false, 2, "Corolla Combi" },
                    { 80, false, 3, "Avella" },
                    { 56, false, 2, "Corolla" },
                    { 81, false, 3, "Besta" },
                    { 83, false, 3, "Carnival" },
                    { 106, false, 4, "G" },
                    { 105, false, 4, "FX" },
                    { 104, false, 4, "EX" },
                    { 103, false, 3, "Venga" },
                    { 102, false, 3, "Sportage" },
                    { 101, false, 3, "Soul" },
                    { 100, false, 3, "Sorento" },
                    { 99, false, 3, "Shuma" },
                    { 98, false, 3, "Sephia" },
                    { 97, false, 3, "Rio sedan" },
                    { 96, false, 3, "Rio Combi" },
                    { 95, false, 3, "Rio" },
                    { 94, false, 3, "Pro Cee`d" },
                    { 93, false, 3, "Pride" },
                    { 92, false, 3, "Pregio" },
                    { 91, false, 3, "Picanto" },
                    { 90, false, 3, "Optima" },
                    { 89, false, 3, "Opirus" },
                    { 88, false, 3, "Magentis" },
                    { 87, false, 3, "K 2500" },
                    { 86, false, 3, "Cerato" },
                    { 85, false, 3, "Cee`d SW" },
                    { 84, false, 3, "Cee`d" },
                    { 82, false, 3, "Carens" },
                    { 55, false, 2, "Celica" },
                    { 54, false, 2, "Carina" },
                    { 53, false, 2, "Camry" },
                    { 24, false, 1, "Mondeo" },
                    { 23, false, 1, "Maverick" },
                    { 22, false, 1, "Kuga" },
                    { 21, false, 1, "Ka" },
                    { 20, false, 1, "Grand C-Max" },
                    { 19, false, 1, "Galaxy" },
                    { 18, false, 1, "Fusion" },
                    { 17, false, 1, "Focus kombi" },
                    { 16, false, 1, "Focus CC" },
                    { 15, false, 1, "Focus C-Max" },
                    { 14, false, 1, "Focus" },
                    { 13, false, 1, "Fiesta" },
                    { 12, false, 1, "F-250" },
                    { 11, false, 1, "F-150" },
                    { 10, false, 1, "Explorer" },
                    { 9, false, 1, "Escort kombi" },
                    { 8, false, 1, "Escort Cabrio" },
                    { 7, false, 1, "Escort" },
                    { 6, false, 1, "Edge" },
                    { 5, false, 1, "Cougar" },
                    { 4, false, 1, "Cortina" },
                    { 3, false, 1, "C-Max" },
                    { 2, false, 1, "B-Max" },
                    { 25, false, 1, "Mondeo Combi" },
                    { 26, false, 1, "Mustang" },
                    { 27, false, 1, "Orion" },
                    { 28, false, 1, "Puma" },
                    { 52, false, 2, "Aygo" },
                    { 51, false, 2, "Avensis Van Verso" },
                    { 50, false, 2, "Avensis Combi" },
                    { 49, false, 2, "Avensis" },
                    { 48, false, 2, "Auris" },
                    { 47, false, 2, "4-Runner" },
                    { 46, false, 1, "Windstar" },
                    { 45, false, 1, "Transit Van 350" },
                    { 44, false, 1, "Transit Van" },
                    { 43, false, 1, "Transit Valnik" },
                    { 42, false, 1, "Transit Tourneo" },
                    { 107, false, 4, "G Coupe" },
                    { 41, false, 1, "Transit kombi" },
                    { 39, false, 1, "Transit Courier" },
                    { 38, false, 1, "Transit Connect LWB" },
                    { 37, false, 1, "Transit Bus" },
                    { 36, false, 1, "Transit" },
                    { 35, false, 1, "Transit" },
                    { 34, false, 1, "Tourneo Custom" },
                    { 33, false, 1, "Tourneo Connect" },
                    { 32, false, 1, "Street Ka" },
                    { 31, false, 1, "Sierra" },
                    { 30, false, 1, "S-Max" },
                    { 29, false, 1, "Ranger" },
                    { 40, false, 1, "Transit Custom" },
                    { 108, false, 4, "M" },
                    { 109, false, 4, "Q" },
                    { 110, false, 4, "QX" },
                    { 188, false, 7, "IS" },
                    { 187, false, 7, "GX" },
                    { 186, false, 7, "GS 300" },
                    { 185, false, 7, "GS" },
                    { 184, false, 7, "CT" },
                    { 183, false, 6, "Space Star" },
                    { 182, false, 6, "Pajero Wagon" },
                    { 181, false, 6, "Pajero Sport" },
                    { 180, false, 6, "Pajero Pinin Wagon" },
                    { 179, false, 6, "Pajeto Pinin" },
                    { 178, false, 6, "Pajero" },
                    { 177, false, 6, "Outlander" },
                    { 176, false, 6, "Lancer Sportback" },
                    { 175, false, 6, "Lancer Evo" },
                    { 174, false, 6, "Lancer Combi" },
                    { 173, false, 6, "Lancer" },
                    { 172, false, 6, "L300" },
                    { 171, false, 6, "L200 Pick up Allrad" },
                    { 170, false, 6, "L200 Pick up" },
                    { 169, false, 6, "L200" },
                    { 168, false, 6, "Grandis" },
                    { 167, false, 6, "Galant Combi" },
                    { 166, false, 6, "Galant" },
                    { 189, false, 7, "IS 200" },
                    { 190, false, 7, "IS 250 C" },
                    { 191, false, 7, "IS-F" },
                    { 192, false, 7, "LS" },
                    { 216, false, 9, "108" },
                    { 215, false, 9, "106" },
                    { 214, false, 9, "107" },
                    { 213, false, 9, "1007" },
                    { 212, false, 8, "Panamera" },
                    { 211, false, 8, "Macan" },
                    { 210, false, 8, "Cayman" },
                    { 209, false, 8, "Cayenne" },
                    { 208, false, 8, "Boxster" },
                    { 207, false, 8, "997" },
                    { 206, false, 8, "944" },
                    { 165, false, 6, "Fuso canter" },
                    { 205, false, 8, "924" },
                    { 203, false, 8, "911 Targa" },
                    { 202, false, 8, "911 Carrera Cabrio" },
                    { 201, false, 8, "911 Carrera" },
                    { 200, false, 7, "SC 430" },
                    { 199, false, 7, "RX 450h" },
                    { 198, false, 7, "RX 400h" },
                    { 197, false, 7, "RX 300" },
                    { 196, false, 7, "RX" },
                    { 195, false, 7, "RC F" },
                    { 194, false, 7, "NX" },
                    { 193, false, 7, "LX" },
                    { 204, false, 8, "911 Turbo" },
                    { 218, false, 9, "205" },
                    { 164, false, 6, "Eclipse" },
                    { 162, false, 6, "Colt" },
                    { 133, false, 5, "LT" },
                    { 132, false, 5, "Jetta" },
                    { 131, false, 5, "Golf Variant" },
                    { 130, false, 5, "Golf Sportvan" },
                    { 129, false, 5, "Golf Plus" },
                    { 128, false, 5, "Golf Cabrio" },
                    { 127, false, 5, "Golf" },
                    { 126, false, 5, "Fox" },
                    { 125, false, 5, "Eos" },
                    { 124, false, 5, "CrossTouran" },
                    { 123, false, 5, "Crafter Kombi" },
                    { 122, false, 5, "Crafter Van" },
                    { 121, false, 5, "Crafter" },
                    { 120, false, 5, "CC" },
                    { 119, false, 5, "Caravelle" },
                    { 118, false, 5, "California" },
                    { 117, false, 5, "Life" },
                    { 116, false, 5, "Caddy Van" },
                    { 115, false, 5, "Caddy" },
                    { 114, false, 5, "Bora Variant" },
                    { 113, false, 5, "Bora" },
                    { 112, false, 5, "Beetle" },
                    { 111, false, 5, "Amarok" },
                    { 134, false, 5, "Lupo" },
                    { 135, false, 5, "Multivan" },
                    { 136, false, 5, "New Beetle" },
                    { 137, false, 5, "New Beetle Cabrio" },
                    { 161, false, 6, "Carisma" },
                    { 160, false, 6, "ASX" },
                    { 159, false, 6, "3000 GT" },
                    { 158, false, 5, "Touran" },
                    { 157, false, 5, "Touareg" },
                    { 156, false, 5, "Tiguan" },
                    { 155, false, 5, "T5 Transporter Shuttle" },
                    { 154, false, 5, "T5 Multivan" },
                    { 153, false, 5, "T5 Caravelle" },
                    { 152, false, 5, "T5" },
                    { 151, false, 5, "T4 Multivan" },
                    { 163, false, 6, "Colt CC" },
                    { 150, false, 5, "T4 Caravelle" },
                    { 148, false, 5, "Sharan" },
                    { 147, false, 5, "Scirocco" },
                    { 146, false, 5, "Polo Variant" },
                    { 145, false, 5, "Polo Van" },
                    { 144, false, 5, "Polo" },
                    { 143, false, 5, "Phaeton" },
                    { 142, false, 5, "Passat Variant Van" },
                    { 141, false, 5, "Passat Variant" },
                    { 140, false, 5, "Passat CC" },
                    { 139, false, 5, "Passat Alltrack" },
                    { 138, false, 5, "Passat" },
                    { 149, false, 5, "T4" },
                    { 1, false, 1, "Aerostar" }
                });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "CarId", "IsDeleted", "LicensePlate", "ModelId", "UserId", "VehicleIDNumber", "YearOfManufacture" },
                values: new object[] { 1, false, "XY9546XY", 14, 1, "3C63D2EL7CG096460", 2005 });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "CarId", "IsDeleted", "LicensePlate", "ModelId", "UserId", "VehicleIDNumber", "YearOfManufacture" },
                values: new object[] { 2, false, "CX0750XC", 55, 2, "WBA3V5C50EJ731030", 2009 });

            migrationBuilder.InsertData(
                table: "Cars",
                columns: new[] { "CarId", "IsDeleted", "LicensePlate", "ModelId", "UserId", "VehicleIDNumber", "YearOfManufacture" },
                values: new object[] { 3, false, "YY6970PC", 66, 2, "WAUPEAFM1DA501217", 2008 });

            migrationBuilder.InsertData(
                table: "ServiceHistories",
                columns: new[] { "ServiceHistoryId", "CarId", "IsDeleted" },
                values: new object[] { 1, 2, false });

            migrationBuilder.InsertData(
                table: "ServiceHistories",
                columns: new[] { "ServiceHistoryId", "CarId", "IsDeleted" },
                values: new object[] { 2, 3, false });

            migrationBuilder.InsertData(
                table: "Orders",
                columns: new[] { "OrderId", "IsDeleted", "OrderDate", "ServiceHistoryId" },
                values: new object[,]
                {
                    { 1, false, new DateTime(2020, 8, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 2, false, new DateTime(2020, 5, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), 1 },
                    { 3, false, new DateTime(2021, 5, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 },
                    { 4, false, new DateTime(2020, 6, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Cars_LicensePlate",
                table: "Cars",
                column: "LicensePlate",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cars_ModelId",
                table: "Cars",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_Cars_UserId",
                table: "Cars",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Cars_VehicleIDNumber",
                table: "Cars",
                column: "VehicleIDNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Models_ManufacturerId",
                table: "Models",
                column: "ManufacturerId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ServiceHistoryId",
                table: "Orders",
                column: "ServiceHistoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceHistories_CarId",
                table: "ServiceHistories",
                column: "CarId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Services_OrderId",
                table: "Services",
                column: "OrderId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Services");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "ServiceHistories");

            migrationBuilder.DropTable(
                name: "Cars");

            migrationBuilder.DropTable(
                name: "Models");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Manufacturers");
        }
    }
}
