﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VirtualCarGarage.Data.Models
{
    public class Service
    {
        [Key]
        public int ServiceId { get; set; }
        [Required, MaxLength(100)]
        public string Name { get; set; }
        [Required, Range(0.0, 999.9), Column(TypeName = "decimal(5,2)")]
        public decimal Price { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
    }
}
