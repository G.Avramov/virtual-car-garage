﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VirtualCarGarage.Data.Models
{
    public class Manufacturer
    {
        [Key]
        public int ManufacturerId { get; set; }
        [Required, MaxLength(100)]
        public string Name { get; set; }
        public List<Model> Models { get; set; } = new List<Model>();
        public bool IsDeleted { get; set; }
    }
}
