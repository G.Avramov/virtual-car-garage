﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VirtualCarGarage.Data.Models
{
    public class User : IdentityUser<int>
    {
        [Required, MinLength(2), MaxLength(20)]
        public string FirstName { get; set; }

        [Required, MinLength(2), MaxLength(20)]
        public string LastName { get; set; }
        public List<Car> Cars { get; set; } = new List<Car>();
        public bool IsDeleted { get; set; }
    }
}
