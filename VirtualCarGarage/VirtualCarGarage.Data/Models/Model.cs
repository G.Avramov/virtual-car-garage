﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VirtualCarGarage.Data.Models
{
    public class Model
    {
        [Key]
        public int ModelId { get; set; }
        [Required, MaxLength(100)]
        public string Name { get; set; }
        [ForeignKey("Manufacturer")]
        public int ManufacturerId { get; set; }
        [Required]
        public Manufacturer Manufacturer { get; set; }
        public bool IsDeleted { get; set; }
    }
}
