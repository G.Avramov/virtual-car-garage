﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VirtualCarGarage.Data.Models
{
    public class Order
    {
        [Key]
        public int OrderId { get; set; }
        [Required]
        [Column(TypeName = "date")]
        public DateTime OrderDate { get; set; }
        [ForeignKey("ServiceHistory")]
        public int ServiceHistoryId { get; set; }
        public ServiceHistory ServiceHistory { get; set; }
        public List<Service> ServicesOrdered { get; set; } = new List<Service>();
        public bool IsDeleted { get; set; }
    }
}
