﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VirtualCarGarage.Data.Models
{
    public class Car
    {
        [Key]
        public int CarId { get; set; }
        [ForeignKey("User")]
        public int UserId { get; set; }
        [Required]
        public User User { get; set; }

        [ForeignKey("Model")]
        public int ModelId { get; set; }
        [Required]
        public Model Model { get; set; }
        [Required, MaxLength(10)]
        public string LicensePlate { get; set; }
        [Required]
        public int YearOfManufacture { get; set; }
        [Required, MinLength(17), MaxLength(17)]
        public string VehicleIDNumber { get; set; }
        [Required]
        public ServiceHistory ServiceHistory { get; set; }
        public bool IsDeleted { get; set; }
    }
}
