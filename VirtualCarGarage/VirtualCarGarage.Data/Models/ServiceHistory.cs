﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VirtualCarGarage.Data.Models
{
    public class ServiceHistory
    {
        [Key]
        public int ServiceHistoryId { get; set; }
        [ForeignKey("Car")]
        public int CarId { get; set; }
        [Required]
        public Car Car { get; set; }
        public List<Order> Orders { get; set; } = new List<Order>();
        public bool IsDeleted { get; set; }
    }
}
