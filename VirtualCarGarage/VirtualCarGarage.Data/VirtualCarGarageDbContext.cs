﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using VirtualCarGarage.Data.Models;

namespace VirtualCarGarage.Data
{
    public class VirtualCarGarageDbContext : IdentityDbContext<User, Role, int>
    {
        public VirtualCarGarageDbContext(DbContextOptions<VirtualCarGarageDbContext> options) : base(options)
        {
        }

        public DbSet<Car> Cars { get; set; }
        public DbSet<Manufacturer> Manufacturers { get; set; }
        public DbSet<Model> Models { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Service> Services { get; set; }
        public DbSet<ServiceHistory> ServiceHistories { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Car>().HasIndex(v => v.LicensePlate).IsUnique();
            builder.Entity<Car>().HasIndex(v => v.VehicleIDNumber).IsUnique();

           /* builder.Entity<UserName>(u =>
            {
                u.HasKey(b => b.NameId);
                u.Property(b => b.NameId).ValueGeneratedOnAdd();
            });*/
            /*var cascadeFKs = builder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);*/

           // Seed(builder);
            base.OnModelCreating(builder);
        }

        private static void Seed(ModelBuilder builder)
        {
            builder.Entity<Role>().HasData(
                new Role() { Id = 1, Name = "Admin", NormalizedName = "ADMIN" },
                new Role() { Id = 2, Name = "Customer", NormalizedName = "CUSTOMER" }
                );

            /*builder.Entity<Car>()
            .HasIndex(b => b.LicensePlate)
            .IsUnique();*/
            var passHasher = new PasswordHasher<User>();

            //var users = new List<User>()
            // Admin
            var adminUser = new User();
            adminUser.Id = 1;
            adminUser.UserName = "admin@admin.com";
            adminUser.NormalizedUserName = "ADMIN@ADMIN.COM";
            adminUser.FirstName = "Ivan";
            adminUser.LastName = "Ivanov";
            adminUser.PhoneNumber = "1234567890";
            adminUser.Email = "admin@admin.com";
            adminUser.NormalizedEmail = "ADMIN@ADMIN.COM";
            adminUser.PasswordHash = passHasher.HashPassword(adminUser, "admin123");
            adminUser.SecurityStamp = Guid.NewGuid().ToString();
            builder.Entity<User>().HasData(adminUser);

            // Link Role & User (for Admin)
            var adminUserRole = new IdentityUserRole<int>();
            adminUserRole.RoleId = 1;
            adminUserRole.UserId = adminUser.Id;
            builder.Entity<IdentityUserRole<int>>().HasData(adminUserRole);

            var customerUser = new User();
            customerUser.Id = 2;
            customerUser.UserName = "customer@customer.com";
            customerUser.NormalizedUserName = "CUSTOMER@CUSTOMER.COM";
            customerUser.FirstName = "Tosho";
            customerUser.LastName = "Toshev";
            customerUser.PhoneNumber = "5555599999";
            customerUser.Email = "customer@customer.com";
            customerUser.NormalizedEmail = "CUSTOMER@CUSTOMER.COM";
            customerUser.PasswordHash = passHasher.HashPassword(customerUser, "customer123");
            customerUser.SecurityStamp = Guid.NewGuid().ToString();
            builder.Entity<User>().HasData(customerUser);

            // Link Role & User (for Regular User)
            var customerUserRole = new IdentityUserRole<int>();
            customerUserRole.RoleId = 2;
            customerUserRole.UserId = customerUser.Id;
            builder.Entity<IdentityUserRole<int>>().HasData(customerUserRole);

            var assemblyPath = Environment.CurrentDirectory;
            var seedFilesPath = @"\..\VirtualCarGarage.Data\SeedData\";
            var fullPath = assemblyPath + seedFilesPath;

            var manufacturers = JsonConvert.DeserializeObject<List<Manufacturer>>(File.ReadAllText(fullPath + "makes.json"));
            var models = JsonConvert.DeserializeObject<List<Model>>(File.ReadAllText(fullPath + "models.json"));
            var orders = JsonConvert.DeserializeObject<List<Order>>(File.ReadAllText(fullPath + "orders.json"));
            var services = JsonConvert.DeserializeObject<List<Service>>(File.ReadAllText(fullPath + "services.json"));
            var serviceHistories = JsonConvert.DeserializeObject<List<ServiceHistory>>(File.ReadAllText(fullPath + "serviceHistories.json"));
            var cars = JsonConvert.DeserializeObject<List<Car>>(File.ReadAllText(fullPath + "cars.json"));

            builder.Entity<Manufacturer>().HasData(manufacturers);
            builder.Entity<Model>().HasData(models);
            builder.Entity<Service>().HasData(services);
            builder.Entity<Order>().HasData(orders);
            builder.Entity<ServiceHistory>().HasData(serviceHistories);
            builder.Entity<Car>().HasData(cars);
        }
    }   
}
