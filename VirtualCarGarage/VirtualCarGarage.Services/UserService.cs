﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Data.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Email;
using VirtualCarGarage.Services.Mappers;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services
{
    public class UserService : IUserService
    {
        private readonly VirtualCarGarageDbContext dbContext;
        private readonly IEmailService emailService;
        public UserService(VirtualCarGarageDbContext dbContext, IEmailService emailService)
        {
            this.dbContext = dbContext;
            this.emailService = emailService;
        }

        public async Task<bool> CreateAsync(UserDTO userDTO)
        {
            var user = await this.dbContext.Users.FirstOrDefaultAsync(u => u.Email == userDTO.Email);

            if (user != null)
            {
                throw new ArgumentException($"User with email: {user.Email} already registered.");
            }

            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();

            var newUser = new User
            {
                Email = userDTO.Email,
                NormalizedEmail = userDTO.Email.ToUpper(),
                UserName = userDTO.Email,
                NormalizedUserName = userDTO.Email.ToUpper(),
                FirstName = userDTO.FirstName,
                LastName = userDTO.LastName,
                PhoneNumber = userDTO.Phone,
                SecurityStamp = Guid.NewGuid().ToString()
            };
            newUser.PasswordHash = passwordHasher.HashPassword(newUser, userDTO.Password);
            await this.dbContext.Users.AddAsync(newUser);
            await this.dbContext.SaveChangesAsync();
            await dbContext.UserRoles.AddAsync(new IdentityUserRole<int> { RoleId = 2, UserId = newUser.Id });

            await this.dbContext.SaveChangesAsync();

            List<string> recipients = new List<string>();
            recipients.Add(userDTO.Email);

            this.emailService.SendEmail(new Message(recipients, "Account registered!", $"Your Username is: {userDTO.Email}, your Password is: {userDTO.Password}."));

            return true;
        }

        public async Task<UserDTO> GetAsync(int userId)
        {
            var user = await this.dbContext.Users
                .Include(u => u.Cars)
                .FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null || user.IsDeleted == true)
            {
                throw new ArgumentException("User not found.");
            }

            return Mapper.MapToUserDTO(user);
        }

        public async Task<List<UserDTO>> GetAllAsync()
        {
            var users = await this.dbContext.Users
                .Where(u => u.IsDeleted == false)
                .Include(u => u.Cars)
                .ToListAsync();

            if (users.Count == 0)
            {
                throw new ArgumentException("No users in database.");
            }

            var userDTOs = new List<UserDTO>();

            foreach (var user in users)
            {
                userDTOs.Add(Mapper.MapToUserDTO(user));
            }

            return userDTOs;
        }

        public async Task<bool> UpdateAsync(UserDTO userDTO)
        {
            var user = await this.dbContext.Users.FirstOrDefaultAsync(u => u.Id == userDTO.Id);

            if (user == null || user.IsDeleted == true)
            {
                throw new ArgumentException($"User with ID: {userDTO.Id} not found.");
            }

            user.FirstName = userDTO.FirstName;
            user.LastName = userDTO.LastName;
            user.PhoneNumber = userDTO.Phone;
            user.Email = userDTO.Email;
            user.NormalizedEmail = userDTO.Email.ToUpper();

            await dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int userId)
        {
            var user = await this.dbContext.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null || user.IsDeleted == true)
            {
                throw new ArgumentException($"User with ID: {userId} not found.");
            }

            user.IsDeleted = true;
            await this.dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> ChangePasswordAsync(int userId, string newPassword, string confirmNewPassword)
        {
            var user = await this.dbContext.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null || user.IsDeleted == true)
            {
                throw new ArgumentException($"No user with ID: {userId} found");
            }

            if (newPassword != confirmNewPassword)
            {
                throw new ArgumentException("Passwords don't match.");
            }

            PasswordHasher<User> passwordHasher = new PasswordHasher<User>();
            user.PasswordHash = passwordHasher.HashPassword(user, newPassword);

            await this.dbContext.SaveChangesAsync();

            return true;
        }
    }
}
