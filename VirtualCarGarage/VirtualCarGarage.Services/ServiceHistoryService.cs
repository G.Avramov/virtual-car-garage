﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Data.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Mappers;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services
{
    public class ServiceHistoryService : IServiceHistoryService
    {
        private readonly VirtualCarGarageDbContext dbContext;

        public ServiceHistoryService(VirtualCarGarageDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<bool> CreateAsync(ServiceHistoryDTO serviceHistoryDTO)
        {
            var car = await this.dbContext.Cars
                .Include(c => c.ServiceHistory)
                .FirstOrDefaultAsync(c => c.CarId == serviceHistoryDTO.CarId && c.IsDeleted == false);

            if (car == null)
            {
                throw new ArgumentException("Invalid Car ID.");
            }

            if (car.ServiceHistory != null && car.ServiceHistory.IsDeleted == false)
            {
                throw new ArgumentException($"Car with ID {car.CarId} already has a ServiceHistory.");
            }
            else if (car.ServiceHistory != null && car.ServiceHistory.IsDeleted == true)
            {
                car.ServiceHistory.IsDeleted = false;
            }
            else
            {
                await this.dbContext.ServiceHistories.AddAsync(new ServiceHistory
                {
                    CarId = serviceHistoryDTO.CarId
                });
            }

            await this.dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<ServiceHistoryDTO> GetAsync(int serviceHistoryId)
        {
            var serviceHistory = await this.dbContext.ServiceHistories
                .Include(sh => sh.Car)
                    .ThenInclude(c => c.Model)
                        .ThenInclude(m => m.Manufacturer)
                .Include(sh => sh.Orders)
                    .ThenInclude(o => o.ServicesOrdered)
                .FirstOrDefaultAsync(sh => sh.ServiceHistoryId == serviceHistoryId && sh.IsDeleted == false);

            if (serviceHistory == null)
            {
                throw new ArgumentException($"ServiceHistory with ID: {serviceHistoryId} not found.");
            }

            return Mapper.MapToServiceHistoryDTO(serviceHistory);
        }

        public async Task<List<ServiceHistoryDTO>> GetByUserIdAsync(int userId)
        {
            var userCars = await this.dbContext.Cars
                .Include(c => c.ServiceHistory)
                    .ThenInclude(sh => sh.Orders)
                .Where(c => c.UserId == userId && c.IsDeleted == false).ToListAsync();         

            if(userCars.Count == 0)
            {
                throw new ArgumentException("No cars registered.");
            }

            List<ServiceHistoryDTO> serviceHistoryDTOs = new List<ServiceHistoryDTO>();

            foreach (var car in userCars)
            {
                serviceHistoryDTOs.Add(Mapper.MapToServiceHistoryDTO(car.ServiceHistory));
            }

            return serviceHistoryDTOs;
        }

        public async Task<List<ServiceHistoryDTO>> GetAllAsync()
        {
            var serviceHistories = await this.dbContext.ServiceHistories
                .Where(sh => sh.IsDeleted == false)
                .Include(sh => sh.Car)
                    .ThenInclude(c => c.Model)
                        .ThenInclude(m => m.Manufacturer)
                .Include(sh => sh.Orders)
                    .ThenInclude(o => o.ServicesOrdered)
                .ToListAsync();

            if (serviceHistories.Count() == 0)
            {
                throw new ArgumentException("No ServiceHistories found.");
            }

            List<ServiceHistoryDTO> serviceHistoryDTOs = new List<ServiceHistoryDTO>();

            foreach (ServiceHistory serviceHistory in serviceHistories)
            {
                serviceHistoryDTOs.Add(Mapper.MapToServiceHistoryDTO(serviceHistory));
            }

            return serviceHistoryDTOs;
        }

        public async Task<bool> UpdateAsync(ServiceHistoryDTO serviceHistoryDTO)
        {
            var serviceHistory = await this.dbContext.ServiceHistories
                .FirstOrDefaultAsync(sh => sh.ServiceHistoryId == serviceHistoryDTO.Id && sh.IsDeleted == false);

            if (serviceHistory == null)
            {
                throw new ArgumentException($"ServiceHistory with ID: {serviceHistoryDTO.Id} not found.");
            }

            var car = await this.dbContext.Cars
                .Include(c => c.ServiceHistory)
                .FirstOrDefaultAsync(c => c.CarId == serviceHistoryDTO.CarId && c.IsDeleted == false);

            if (car == null)
            {
                throw new ArgumentException($"Car with ID: {serviceHistoryDTO.CarId} not found.");
            }

            if(car.ServiceHistory != null && car.ServiceHistory.IsDeleted == false) 
            {
                throw new ArgumentException($"Car with ID: {car.CarId} already has a ServiceHistory.");
            }

            serviceHistory.CarId = car.CarId;

            this.dbContext.ServiceHistories.Update(serviceHistory);
            await this.dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int serviceHistoryId)
        {
            var serviceHistory = await this.dbContext.ServiceHistories
                .FirstOrDefaultAsync(sh => sh.ServiceHistoryId == serviceHistoryId && sh.IsDeleted == false);

            if (serviceHistory == null)
            {
                throw new ArgumentException($"ServiceHistory with ID: {serviceHistoryId} not found.");
            }

            serviceHistory.IsDeleted = true;

            this.dbContext.ServiceHistories.Update(serviceHistory);
            await this.dbContext.SaveChangesAsync();

            return true;
        }       
    }
}
