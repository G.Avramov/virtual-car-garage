﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Data.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Mappers;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services
{
    public class ModelService : IModelService
    {
        private readonly VirtualCarGarageDbContext dbContext;

        public ModelService(VirtualCarGarageDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<bool> CreateAsync(ModelDTO modelDTO)
        {
            var manufacturer = await this.dbContext.Manufacturers
                .FirstOrDefaultAsync(m => m.IsDeleted == false && m.ManufacturerId == modelDTO.ManufacturerId);

            if (manufacturer == null)
            {
                throw new ArgumentException($"Manufacturer with ID: {modelDTO.ManufacturerId} not found.");
            }

            var model = await this.dbContext.Models
                .FirstOrDefaultAsync(m => m.ManufacturerId == manufacturer.ManufacturerId && m.Name == modelDTO.Name);

            if (model == null)
            {
                await this.dbContext.Models.AddAsync(new Model
                {
                    Name = modelDTO.Name,
                    Manufacturer = manufacturer
                });
                await this.dbContext.SaveChangesAsync();

                return true;
            }
            else if (model.IsDeleted == true)
            {
                model.IsDeleted = false;
                dbContext.Models.Update(model);
                await this.dbContext.SaveChangesAsync();

                return true;
            }

            throw new ArgumentException($"Model with name: {modelDTO.Name} already exists.");
        }

        public async Task<ModelDTO> GetAsync(int modelId)
        {
            var model = await this.dbContext.Models
                .Include(m => m.Manufacturer)
                .FirstOrDefaultAsync(m => m.ModelId == modelId && m.IsDeleted == false);

            if (model == null)
            {
                throw new ArgumentException($"Model with ID: {modelId} not found.");
            }

            return Mapper.MapToModelDTO(model);
        }

        public async Task<List<ModelDTO>> GetAllAsync()
        {
            var models = await this.dbContext.Models
                .Where(m => m.IsDeleted == false)
                .ToListAsync();

            if (models.Count == 0)
            {
                throw new ArgumentException("No models found.");
            }

            List<ModelDTO> modelDTOs = new List<ModelDTO>();

            foreach (Model model in models)
            {
                modelDTOs.Add(Mapper.MapToModelDTO(model));
            }

            return modelDTOs;
        }

        public async Task<bool> UpdateAsync(ModelDTO modelDTO)
        {
            var model = await this.dbContext.Models
                .Include(m => m.Manufacturer)
                .FirstOrDefaultAsync(m => m.ModelId == modelDTO.Id && m.IsDeleted == false);

            if (model == null)
            {
                throw new ArgumentException($"Model with ID: {modelDTO.Id} not found.");
            }

            model.Name = modelDTO.Name;

            await this.dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int modelId)
        {
            var model = await this.dbContext.Models
                .FirstOrDefaultAsync(m => m.ModelId == modelId && m.IsDeleted == false);

            if (model == null)
            {
                throw new ArgumentException($"Model with ID: {modelId} not found.");
            }

            model.IsDeleted = true;

            await this.dbContext.SaveChangesAsync();

            return true;
        }
    }
}
