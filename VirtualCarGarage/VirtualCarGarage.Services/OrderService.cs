﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Data.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Mappers;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services
{
    public class OrderService : IOrderService
    {
        private readonly VirtualCarGarageDbContext dbContext;

        public OrderService(VirtualCarGarageDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<bool> CreateAsync(OrderDTO orderDTO)
        {
            var serviceHistory = await this.dbContext.ServiceHistories.FirstOrDefaultAsync(sh => sh.ServiceHistoryId == orderDTO.ServiceHistoryId);

            if(serviceHistory == null)
            {
                throw new ArgumentException("Invalid ServiceHistory ID.");
            }
            //not suer if restore is okk
            if(serviceHistory.IsDeleted == true)
            {
                serviceHistory.IsDeleted = false;

                await this.dbContext.SaveChangesAsync();
            }

            await this.dbContext.Orders.AddAsync(new Order
            {
                OrderDate = DateTime.Now,
                ServiceHistoryId = serviceHistory.ServiceHistoryId
            });

            await this.dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<OrderDTO> GetAsync(int orderId)
        {
            var order = await this.dbContext.Orders
                .Include(o => o.ServicesOrdered)
                .FirstOrDefaultAsync(o => o.OrderId == orderId && o.IsDeleted == false);

            if (order == null)
            {
                throw new ArgumentException($"Order with ID: {orderId} not found.");
            }

            return Mapper.MapToOrderDTO(order);
        }

        public async Task<List<OrderDTO>> GetAllAsync()
        {
            var orders = await this.dbContext.Orders
                .Include(o => o.ServicesOrdered)
                .Where(o => o.IsDeleted == false)
                .ToListAsync();

            if (orders.Count() == 0)
            {
                throw new ArgumentException("No orders found.");
            }

            List<OrderDTO> orderDTOs = new List<OrderDTO>();

            foreach (Order order in orders)
            {
                orderDTOs.Add(Mapper.MapToOrderDTO(order));
            }

            return orderDTOs;
        }

        public async Task<bool> AddServices(int id, List<ServiceDTO> servicesDTO)
        {
            var order = await this.dbContext.Orders
                .Include(o => o.ServicesOrdered)
                .FirstOrDefaultAsync(o => o.OrderId == id && o.IsDeleted == false);

            if (order == null)
            {
                throw new ArgumentException($"Order with ID: {id} not found.");
            }

            foreach (var item in servicesDTO)
            {
                order.ServicesOrdered.Add(new Service
                {
                    ServiceId = item.Id,
                    Name = item.Name,
                    Price = item.Price
                });
            }

            this.dbContext.Orders.Update(order);
            await this.dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> RemoveServices(int id, List<ServiceDTO> servicesDTO)
        {
            var order = await this.dbContext.Orders
                .Include(o => o.ServicesOrdered)
                .FirstOrDefaultAsync(o => o.OrderId == id && o.IsDeleted == false);

            if (order == null)
            {
                throw new ArgumentException($"Order with ID: {id} not found.");
            }

            var servicesToRemove = new List<Service>();

            foreach (var item in servicesDTO)
            {
                servicesToRemove.Add(new Service
                {
                    ServiceId = item.Id,
                    Name = item.Name,
                    Price = item.Price
                });
            }

            for (int i = 0; i < order.ServicesOrdered.Count; i++)
            {
                if(servicesToRemove.Any(str => str.Name == order.ServicesOrdered[i].Name))
                {
                    order.ServicesOrdered.RemoveAt(i);
                    i--;
                }
            }
            

            this.dbContext.Orders.Update(order);
            await this.dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateAsync(OrderEditDTO orderEditDTO)
        {
            var order = await this.dbContext.Orders
                .FirstOrDefaultAsync(o => o.OrderId == orderEditDTO.Id && o.IsDeleted == false);

            if (order == null)
            {
                throw new ArgumentException($"Order with ID: {orderEditDTO.Id} not found.");
            }

            var serviceHistory = await this.dbContext.ServiceHistories
                .FirstOrDefaultAsync(sh => sh.ServiceHistoryId == orderEditDTO.ServiceHistoryId && sh.IsDeleted == false);

            if(serviceHistory == null)
            {
                throw new ArgumentException($"ServiceHistory with ID: {orderEditDTO.ServiceHistoryId} not found.");
            }

            order.OrderDate = orderEditDTO.OrderDate;
            order.ServiceHistoryId = orderEditDTO.ServiceHistoryId;

           /* var services = new List<Service>();
            foreach (var item in orderDTO.Services)
            {
                services.Add(new Service
                {
                    ServiceId = item.Id,
                    Name = item.Name,
                    Price = item.Price
                });
            }
            order.ServicesOrdered = services;*/

            this.dbContext.Orders.Update(order);
            await this.dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int orderId)
        {
            var order = await this.dbContext.Orders
                .FirstOrDefaultAsync(o => o.OrderId == orderId && o.IsDeleted == false);

            if (order == null)
            {
                throw new ArgumentException($"Order with ID: {orderId} not found.");
            }

            order.IsDeleted = true;

            this.dbContext.Orders.Update(order);
            await this.dbContext.SaveChangesAsync();

            return true;
        }
    }
}
