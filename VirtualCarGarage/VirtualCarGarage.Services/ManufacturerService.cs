﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Data.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Mappers;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services
{
    public class ManufacturerService : IManufacturerService
    {
        private readonly VirtualCarGarageDbContext dbContext;

        public ManufacturerService(VirtualCarGarageDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<bool> CreateAsync(ManufacturerDTO manufacturerDTO)
        {
            var manufacturer = await this.dbContext.Manufacturers
                .FirstOrDefaultAsync(m => m.Name == manufacturerDTO.Name);

            if (manufacturer == null)
            {
                await this.dbContext.Manufacturers.AddAsync(new Manufacturer
                {
                    Name = manufacturerDTO.Name
                });

                await this.dbContext.SaveChangesAsync();

                return true;
            }
            else if (manufacturer.IsDeleted == true)
            {
                manufacturer.IsDeleted = false;

                await this.dbContext.SaveChangesAsync();

                return true;
            }

            throw new ArgumentException($"Manufacturer with name: {manufacturerDTO.Name} already exists.");
        }

        public async Task<ManufacturerDTO> GetAsync(int manufacturerId)
        {
            var manufacturer = await this.dbContext.Manufacturers
                .FirstOrDefaultAsync(m => m.ManufacturerId == manufacturerId && m.IsDeleted == false);

            if (manufacturer == null)
            {
                throw new ArgumentException($"Manufacturer with ID: {manufacturerId} not found.");
            }

            return Mapper.MapToManufacturerDTO(manufacturer);
        }

        public async Task<List<ManufacturerDTO>> GetAllAsync()
        {
            var manufacturers = await this.dbContext.Manufacturers
                .Where(m => m.IsDeleted == false)
                .ToListAsync();

            if (manufacturers.Count() == 0)
            {
                throw new ArgumentException("No manufactures found.");
            }

            List<ManufacturerDTO> manufacturerDTOs = new List<ManufacturerDTO>();

            foreach (Manufacturer manufacturer in manufacturers)
            {
                manufacturerDTOs.Add(Mapper.MapToManufacturerDTO(manufacturer));
            }

            return manufacturerDTOs;
        }

        public async Task<bool> UpdateAsync(ManufacturerDTO manufacturerDTO)
        {
            var manufacturer = await this.dbContext.Manufacturers
                .FirstOrDefaultAsync(m => m.ManufacturerId == manufacturerDTO.Id && m.IsDeleted == false);

            if (manufacturer == null)
            {
                throw new ArgumentException($"Manufacturer with ID: {manufacturerDTO.Id} not found.");
            }


            manufacturer.Name = manufacturerDTO.Name;

            await this.dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int manufacturerId)
        {
            var manufacturer = await this.dbContext.Manufacturers
                .FirstOrDefaultAsync(m => m.ManufacturerId == manufacturerId && m.IsDeleted == false);

            if (manufacturer == null)
            {
                throw new ArgumentException($"Manufacturer with ID: {manufacturerId} not found.");
            }

            manufacturer.IsDeleted = true;

            this.dbContext.Manufacturers.Update(manufacturer);
            await this.dbContext.SaveChangesAsync();

            return true;
        }
    }
}
