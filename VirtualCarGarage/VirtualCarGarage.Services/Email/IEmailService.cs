﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualCarGarage.Services.Email
{
    public interface IEmailService
    {
        void SendEmail(Message message);
    }
}
