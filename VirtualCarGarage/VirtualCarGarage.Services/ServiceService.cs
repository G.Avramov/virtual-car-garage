﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Data.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Mappers;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services
{
    public class ServiceService : IServiceService
    {
        private readonly VirtualCarGarageDbContext dbContext;

        public ServiceService(VirtualCarGarageDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<bool> CreateAsync(ServiceDTO serviceDTO)
        {
            var service = await this.dbContext.Services
                .FirstOrDefaultAsync(s => s.Name == serviceDTO.Name);

            if (service == null)
            {
                await this.dbContext.Services.AddAsync(new Service
                {
                    Name = serviceDTO.Name,
                    Price = serviceDTO.Price
                });

                await this.dbContext.SaveChangesAsync();

                return true;
            }
            else if (service.IsDeleted == true)
            {
                service.IsDeleted = false;

                await this.dbContext.SaveChangesAsync();

                return true;
            }

            throw new ArgumentException($"Service with name: {serviceDTO.Name} already exists.");
        }

        public async Task<ServiceDTO> GetAsync(int serviceId)
        {
            var service = await this.dbContext.Services
                .FirstOrDefaultAsync(s => s.ServiceId == serviceId && s.IsDeleted == false);

            if (service == null)
            {
                throw new ArgumentException($"Service with ID: {serviceId} not found.");
            }

            return Mapper.MapToServiceDTO(service);
        }

        public async Task<List<ServiceDTO>> GetAllAsync()
        {
            var services = await this.dbContext.Services
                .Where(s => s.IsDeleted == false)
                .ToListAsync();

            if (services.Count() == 0)
            {
                throw new ArgumentException("No services found.");
            }

            List<ServiceDTO> serviceDTOs = new List<ServiceDTO>();

            foreach (Service service in services)
            {
                serviceDTOs.Add(Mapper.MapToServiceDTO(service));
            }

            return serviceDTOs;
        }

        public async Task<bool> UpdateAsync(ServiceDTO serviceDTO)
        {
            var service = await this.dbContext.Services
                .FirstOrDefaultAsync(s => s.ServiceId == serviceDTO.Id && s.IsDeleted == false);

            if (service == null)
            {
                throw new ArgumentException($"Service with ID: {serviceDTO.Id} not found.");
            }

            service.Name = serviceDTO.Name;
            service.Price = serviceDTO.Price;

            this.dbContext.Services.Update(service);
            await this.dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int serviceId)
        {
            var service = await this.dbContext.Services
                .FirstOrDefaultAsync(s => s.ServiceId == serviceId && s.IsDeleted == false);

            if (service == null)
            {
                throw new ArgumentException($"Service with ID: {serviceId} not found.");
            }

            service.IsDeleted = true;

            this.dbContext.Services.Update(service);
            await this.dbContext.SaveChangesAsync();

            return true;
        }       
    }
}
