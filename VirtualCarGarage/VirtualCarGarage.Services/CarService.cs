﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Data.Models;
using VirtualCarGarage.Services.Contracts;
using VirtualCarGarage.Services.Mappers;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services
{
    public class CarService : ICarService
    {
        private readonly VirtualCarGarageDbContext dbContext;

        public CarService(VirtualCarGarageDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<bool> CreateAsync(CarDTO carDTO)
        {
            var car = await this.dbContext.Cars.FirstOrDefaultAsync(c => c.VehicleIDNumber == carDTO.VehicleIDNumber);

            if (car != null && car.IsDeleted)
            {
                car.IsDeleted = false;
                await dbContext.SaveChangesAsync();
                return true;
            }
            else if (car != null && car.IsDeleted == false)
            {
                throw new ArgumentException("Car already exists.");
            }

            var user = await dbContext.Users
                .FirstOrDefaultAsync(u => u.Id == carDTO.UserId && u.IsDeleted == false) ?? throw new ArgumentException("User not found.");

            /*var manufacturer = await dbContext.Manufacturers
               .FirstOrDefaultAsync(m => m.ManufacturerId == carDTO.ManufacturerId);

            if (manufacturer == null)
            {
                manufacturer = new Manufacturer
                {
                    ManufacturerId = carDTO.ManufacturerId
                };

                await dbContext.Manufacturers.AddAsync(manufacturer);
                await dbContext.SaveChangesAsync();
            }
            else if (manufacturer.IsDeleted == true)
            {
                manufacturer.IsDeleted = false;
                await dbContext.SaveChangesAsync();
            }

            var model = await dbContext.Models
              .FirstOrDefaultAsync(m => m.ModelId == carDTO.ModelId);

            if (model == null)
            {
                model = new Model
                {
                    ModelId = carDTO.ModelId,
                    Manufacturer = manufacturer
                };

                await dbContext.Models.AddAsync(model);
                await dbContext.SaveChangesAsync();
            }
            else if (model.IsDeleted == true)
            {
                model.IsDeleted = false;
                await dbContext.SaveChangesAsync();
            }*/

            await dbContext.Cars.AddAsync(new Car
            {
                LicensePlate = carDTO.LicensePlate,
                ModelId = carDTO.ModelId,
                UserId = user.Id,
                VehicleIDNumber = carDTO.VehicleIDNumber,
                YearOfManufacture = carDTO.YearOfManufacture,
                ServiceHistory = new ServiceHistory
                {
                    CarId = carDTO.Id
                }
            });

            await dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<CarDTO> GetAsync(int carId)
        {
            var car = await this.dbContext.Cars
               .Include(c => c.User)
               .Include(c => c.ServiceHistory)
               .Include(c => c.Model)
               .ThenInclude(m => m.Manufacturer)
               .FirstOrDefaultAsync(v => v.CarId == carId && v.IsDeleted == false);

            if (car == null)
            {
                throw new ArgumentException($"Car with ID: {carId} not found.");
            }

            return Mapper.MapToCarDTO(car);
        }

        public async Task<List<CarDTO>> GetAllAsync()
        {
            var cars = await this.dbContext.Cars
                .Include(c => c.User)
                .Include(c => c.ServiceHistory)
                .Include(c => c.Model)
                .ThenInclude(m => m.Manufacturer)
                .Where(v => v.IsDeleted == false)
                .ToListAsync();

            if (cars.Count() == 0)
            {
                throw new ArgumentException("No cars found.");
            }

            List<CarDTO> carDTOs = new List<CarDTO>();

            foreach (Car car in cars)
            {
                carDTOs.Add(Mapper.MapToCarDTO(car));
            }

            return carDTOs;
        }

        public async Task<List<CarWithoutServiceHistoryDTO>> GetAllWithoutServiceHistoryAsync()
        {
            var cars = await this.dbContext.Cars
                .Include(c => c.User)
                .Include(c => c.ServiceHistory)
                .Include(c => c.Model)
                .ThenInclude(m => m.Manufacturer)
                .Where(v => v.IsDeleted == false)
                .ToListAsync();

            if (cars.Count() == 0)
            {
                throw new ArgumentException("No cars found.");
            }

            List<CarWithoutServiceHistoryDTO> carDTOs = new List<CarWithoutServiceHistoryDTO>();

            foreach (Car car in cars.Where(c => c.ServiceHistory == null || c.ServiceHistory.IsDeleted == true))
            {
                carDTOs.Add(Mapper.MapToCarWithoutServiceHistoryDTO(car));
            }

            return carDTOs;
        }

        public async Task<List<CarDTO>> GetAllForCustomerAsync(int userId)
        {
            var cars = await this.dbContext.Cars
                .Include(c => c.User)
                .Include(c => c.ServiceHistory)
                .Include(c => c.Model)
                .ThenInclude(m => m.Manufacturer)
                .Where(c => c.UserId == userId && c.IsDeleted == false)
                .ToListAsync();

            /*if (cars.Count() == 0)
            {
                throw new ArgumentException("No cars found.");
            }*/

            List<CarDTO> carDTOs = new List<CarDTO>();

            foreach (Car car in cars)
            {
                carDTOs.Add(Mapper.MapToCarDTO(car));
            }

            return carDTOs;
        }

        public async Task<bool> UpdateAsync(CarDTO carDTO)
        {
            var car = await dbContext.Cars
                .Include(c => c.User)
                .Include(c => c.Model)
                .ThenInclude(m => m.Manufacturer)
                .Include(c => c.ServiceHistory)
                .FirstOrDefaultAsync(c => c.CarId == carDTO.Id && c.IsDeleted == false);

            if (car == null)
            {
                throw new ArgumentException($"No car with ID: {carDTO.Id} found.");
            }

            /*var user = await dbContext.Users
                .FirstOrDefaultAsync(u => u.Id == carDTO.UserId && u.IsDeleted == false);

            if (user == null)
            {
                throw new ArgumentException($"User must be registered in the system.");
            }*/

            car.LicensePlate = carDTO.LicensePlate;
            car.VehicleIDNumber = carDTO.VehicleIDNumber;
            car.YearOfManufacture = carDTO.YearOfManufacture;

            await dbContext.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int carId)
        {
            var car = await this.dbContext.Cars
                .FirstOrDefaultAsync(c => c.CarId == carId && c.IsDeleted == false);

            if (car == null || car.IsDeleted == true)
            {
                throw new ArgumentException($"Car with ID: {carId} not found.");
            }

            car.IsDeleted = true;

            await this.dbContext.SaveChangesAsync();

            return true;
        }
    }
}
