﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VirtualCarGarage.Data.Models;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services.Mappers
{
    public static class Mapper
    {
        public static ManufacturerDTO MapToManufacturerDTO(Manufacturer manufacturer)
        {
            return new ManufacturerDTO
            {
                Id = manufacturer.ManufacturerId,
                Name = manufacturer.Name
            };
        }

        public static ModelDTO MapToModelDTO(Model model)
        {
            return new ModelDTO
            {
                Id = model.ModelId,
                Name = model.Name
            };
        }

        public static CarDTO MapToCarDTO(Car car)
        {
            return new CarDTO
            {
                Id = car.CarId,
                LicensePlate = car.LicensePlate,
                VehicleIDNumber = car.VehicleIDNumber,
                YearOfManufacture = car.YearOfManufacture,
                UserId = car.UserId,
                ManufacturerId = car.Model.ManufacturerId,
                ModelId = car.ModelId,
                OwnerName = car.User.FirstName + " " + car.User.LastName,
                ServiceHistoryId = car.ServiceHistory.ServiceHistoryId
            };
        }

        public static CarWithoutServiceHistoryDTO MapToCarWithoutServiceHistoryDTO(Car car)
        {
            return new CarWithoutServiceHistoryDTO
            {
                Id = car.CarId,
                LicensePlate = car.LicensePlate,
                VehicleIDNumber = car.VehicleIDNumber,
                YearOfManufacture = car.YearOfManufacture,
                UserId = car.UserId,
                ManufacturerId = car.Model.ManufacturerId,
                ModelId = car.ModelId,
                OwnerName = car.User.FirstName + " " + car.User.LastName
            };
        }

        public static OrderDTO MapToOrderDTO(Order order)
        {
            var services = new List<ServiceDTO>();

            foreach (var item in order.ServicesOrdered)
            {
                services.Add(new ServiceDTO
                {
                    Id = item.ServiceId,
                    Name = item.Name,
                    Price = item.Price
                });
            }
            return new OrderDTO
            {
                Id = order.OrderId,
                OrderDate = order.OrderDate,
                ServiceHistoryId = order.ServiceHistoryId,
                Services = services
            };
        }

        public static ServiceHistoryDTO MapToServiceHistoryDTO(ServiceHistory serviceHistory)
        {
            var ordersDTO = new List<OrderDTO>();
            foreach (var item in serviceHistory.Orders)
            {
                var servicesDTO = new List<ServiceDTO>();
                foreach (var s in item.ServicesOrdered)
                {
                    servicesDTO.Add(new ServiceDTO
                    {
                        Id = s.ServiceId,
                        Name = s.Name,
                        Price = s.Price
                    });
                }
                ordersDTO.Add(new OrderDTO
                {
                    Id = item.OrderId,
                    ServiceHistoryId = item.ServiceHistoryId,
                    OrderDate = item.OrderDate,
                    Services = servicesDTO
                });
            }
           
            return new ServiceHistoryDTO
            {
                Id = serviceHistory.ServiceHistoryId,
                CarId = serviceHistory.CarId,
                Orders = ordersDTO
            };
        }

        public static ServiceDTO MapToServiceDTO(Service service)
        {
            return new ServiceDTO
            {
                Id = service.ServiceId,
                Name = service.Name,
                Price = service.Price
            };
        }

        public static UserDTO MapToUserDTO(User user)
        {
            return new UserDTO
            {
                Id = user.Id,
                Email = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Phone = user.PhoneNumber
            };
        }
    }
}
