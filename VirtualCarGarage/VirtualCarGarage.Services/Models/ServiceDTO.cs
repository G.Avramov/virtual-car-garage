﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualCarGarage.Services.Models
{
    public class ServiceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
