﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualCarGarage.Services.Models
{
    public class ModelDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
    }
}
