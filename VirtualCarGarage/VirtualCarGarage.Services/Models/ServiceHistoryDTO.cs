﻿using System;
using System.Collections.Generic;
using System.Text;
using VirtualCarGarage.Data.Models;

namespace VirtualCarGarage.Services.Models
{
    public class ServiceHistoryDTO
    {
        public int Id { get; set; }
        public int CarId { get; set; }
        public List<OrderDTO> Orders { get; set; }
    }
}
