﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VirtualCarGarage.Services.Models
{
    public class OrderDTO
    {
        public int Id { get; set; }        
        public DateTime OrderDate { get; set; }
        public int ServiceHistoryId { get; set; }
        public List<ServiceDTO> Services { get; set; }
    }

    public class OrderEditDTO
    {
        public int Id { get; set; }
        public DateTime OrderDate { get; set; }
        public int ServiceHistoryId { get; set; }
    }
}
