﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Text.Json.Serialization;
using VirtualCarGarage.Data.Models;

namespace VirtualCarGarage.Services.Models
{
    public class CarDTO
    {
        //[JsonIgnore]
        public int Id { get; set; }
        public int UserId { get; set; }
       // [JsonIgnore]
        public string OwnerName { get; set; }
        public int ModelId { get; set; }

        [MaxLength(10, ErrorMessage = "Invalid License Plate Format.")]
        public string LicensePlate { get; set; }
        public int YearOfManufacture { get; set; }
        [RegularExpression("[A-HJ-NPR-Z0-9]{13}[0-9]{4}", ErrorMessage = "Invalid Vehicle Identification Number Format.")]
        public string VehicleIDNumber { get; set; }
        public int ManufacturerId { get; set; }
        public int ServiceHistoryId { get; set; }
    }

    public class CarWithoutServiceHistoryDTO
    {
        //[JsonIgnore]
        public int Id { get; set; }
        public int UserId { get; set; }
        // [JsonIgnore]
        public string OwnerName { get; set; }
        public int ModelId { get; set; }

        [MaxLength(10, ErrorMessage = "Invalid License Plate Format.")]
        public string LicensePlate { get; set; }
        public int YearOfManufacture { get; set; }
        [StringLength(17, MinimumLength = 17, ErrorMessage = "Invalid VIN Format.")]
        public string VehicleIDNumber { get; set; }
        public int ManufacturerId { get; set; }
    }
}
