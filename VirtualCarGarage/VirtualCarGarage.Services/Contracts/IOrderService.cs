﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services.Contracts
{
    public interface IOrderService
    {
        Task<bool> CreateAsync(OrderDTO orderDTO);
        Task<OrderDTO> GetAsync(int orderId);
        Task<List<OrderDTO>> GetAllAsync();
        Task<bool> AddServices(int id, List<ServiceDTO> servicesDTO);
        Task<bool> RemoveServices(int id, List<ServiceDTO> servicesDTO);
        Task<bool> UpdateAsync(OrderEditDTO orderEditDTO);
        Task<bool> DeleteAsync(int orderId);
    }
}
