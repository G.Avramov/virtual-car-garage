﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services.Contracts
{
    public interface IServiceHistoryService
    {
        Task<bool> CreateAsync(ServiceHistoryDTO serviceHistoryDTO);
        Task<ServiceHistoryDTO> GetAsync(int serviceHistoryId);
        Task<List<ServiceHistoryDTO>> GetAllAsync();
        Task<List<ServiceHistoryDTO>> GetByUserIdAsync(int userId);
        Task<bool> UpdateAsync(ServiceHistoryDTO serviceHistoryDTO);
        Task<bool> DeleteAsync(int serviceHistoryId);
    }
}
