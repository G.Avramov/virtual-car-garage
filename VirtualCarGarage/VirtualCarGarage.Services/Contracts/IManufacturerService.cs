﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services.Contracts
{
    public interface IManufacturerService
    {
        Task<bool> CreateAsync(ManufacturerDTO manufacturerDTO);
        Task<ManufacturerDTO> GetAsync(int manufacturerId);
        Task<List<ManufacturerDTO>> GetAllAsync();
        Task<bool> UpdateAsync(ManufacturerDTO manufacturerDTO);
        Task<bool> DeleteAsync(int manufacturerId);
    }
}
