﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services.Contracts
{
    public interface IUserService
    {
        Task<bool> CreateAsync(UserDTO userDTO);
        Task<UserDTO> GetAsync(int userId);
        Task<List<UserDTO>> GetAllAsync();
        Task<bool> UpdateAsync(UserDTO userDTO);
        Task<bool> DeleteAsync(int userId);
        Task<bool> ChangePasswordAsync(int userId, string newPassword, string confirmNewPassword);
    }
}
