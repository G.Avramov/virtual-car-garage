﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services.Contracts
{
    public interface IServiceService
    {
        Task<bool> CreateAsync(ServiceDTO serviceDTO);
        Task<ServiceDTO> GetAsync(int serviceId);
        Task<List<ServiceDTO>> GetAllAsync();
        Task<bool> UpdateAsync(ServiceDTO serviceDTO);
        Task<bool> DeleteAsync(int serviceId);
    }
}
