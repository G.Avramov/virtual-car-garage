﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services.Contracts
{
    public interface ICarService
    {
        Task<bool> CreateAsync(CarDTO carDTO);
        Task<CarDTO> GetAsync(int carId);
        Task<List<CarDTO>> GetAllAsync();
        Task<List<CarDTO>> GetAllForCustomerAsync(int userId);
        Task<bool> UpdateAsync(CarDTO CarDTO);
        Task<bool> DeleteAsync(int id);
        Task<List<CarWithoutServiceHistoryDTO>> GetAllWithoutServiceHistoryAsync();
    }
}
