﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Services.Contracts
{
    public interface IModelService
    {
        Task<bool> CreateAsync(ModelDTO modelDTO);
        Task<ModelDTO> GetAsync(int modelId);
        Task<List<ModelDTO>> GetAllAsync();
        Task<bool> UpdateAsync(ModelDTO modelDTO);
        Task<bool> DeleteAsync(int modelId);
    }
}
