﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Tests.ManufacturerServiceTests
{
    [TestClass]
    public class UpdateAsync_Should
    {
        [TestMethod]
        public async Task CorrectlyUpdateManufacturer()
        {
            var options = Utils.GetOptions(nameof(CorrectlyUpdateManufacturer));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Manufacturers.AddRangeAsync(Utils.GetManufacturers());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext);
                await sut.UpdateAsync(new ManufacturerDTO
                {
                    Id = 1,
                    Name = "Kia"
                });
                var actual = actContext.Manufacturers.FirstOrDefault(s => s.ManufacturerId == 1);
                Assert.AreEqual(actual.Name, "Kia");
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ManufacturerNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ManufacturerNotFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateAsync(new ManufacturerDTO
                {
                    Id = 1,
                    Name = "Kia"
                }));
            }
        }
    }
}
