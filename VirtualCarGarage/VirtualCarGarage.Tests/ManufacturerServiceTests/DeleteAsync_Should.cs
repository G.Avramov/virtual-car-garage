﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.ManufacturerServiceTests
{
    [TestClass]
    public class DeleteAsync_Should
    {
        [TestMethod]
        public async Task CorrectlyDeleteManufacturer()
        {
            var options = Utils.GetOptions(nameof(CorrectlyDeleteManufacturer));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Manufacturers.AddRangeAsync(Utils.GetManufacturers());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext);
                await sut.DeleteAsync(1);
                var actual = actContext.Manufacturers.FirstOrDefault(s => s.ManufacturerId == 1);
                Assert.AreEqual(actual.IsDeleted, true);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ManufacturerNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ManufacturerNotFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.DeleteAsync(1));
            }
        }
    }
}
