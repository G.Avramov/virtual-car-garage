﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Tests.ManufacturerServiceTests
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public async Task SuccessfullyCreateNewManufacturer()
        {
            var options = Utils.GetOptions(nameof(SuccessfullyCreateNewManufacturer));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext);
                await sut.CreateAsync(new ManufacturerDTO
                {
                    Name = "Kia"
                });
                Assert.AreEqual(actContext.Manufacturers.Count(), 1);
                var actual = actContext.Manufacturers.First();
                Assert.AreEqual(actual.Name, "Kia");
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ManufacturerAlreadyExists()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ManufacturerAlreadyExists));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.AddRangeAsync(Utils.GetManufacturers());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync(new ManufacturerDTO
                {
                    Name = "Audi"
                }));
            }
        }
    }
}
