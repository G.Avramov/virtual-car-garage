﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.ManufacturerServiceTests
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectManufacturer()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectManufacturer));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Manufacturers.AddRangeAsync(Utils.GetManufacturers());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext);
                var actual = sut.GetAsync(1);
                var expected = await actContext.Manufacturers.FirstOrDefaultAsync(s => s.ManufacturerId == 1);
                Assert.AreEqual(actual.Result.Name, expected.Name);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ManufacturerNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ManufacturerNotFound));

            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ManufacturerService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAsync(1));
            }
        }
    }
}
