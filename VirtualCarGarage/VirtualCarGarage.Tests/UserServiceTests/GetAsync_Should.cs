﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Email;
namespace VirtualCarGarage.Tests.UserServiceTests
{
    [TestClass]
    public class GetAsync_Should
    {
        private readonly IEmailService emailService;
        [TestMethod]
        public async Task ReturnCorrectUser()
        {
            
            var options = Utils.GetOptions(nameof(ReturnCorrectUser));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(Utils.GetUsers());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new UserService(actContext, emailService);
                var actual = sut.GetAsync(1);
                var expected = await actContext.Users.FirstOrDefaultAsync(s => s.Id == 1);
                Assert.AreEqual(actual.Result.FirstName, expected.FirstName);
                Assert.AreEqual(actual.Result.LastName, expected.LastName);
                Assert.AreEqual(actual.Result.Email, expected.Email);
                Assert.AreEqual(actual.Result.Phone, expected.PhoneNumber);
            }
        }        

        [TestMethod]
        public async Task ThrowWhen_UserNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_UserNotFound));

            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new UserService(actContext, emailService);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAsync(1));
            }
        }
    }
}
