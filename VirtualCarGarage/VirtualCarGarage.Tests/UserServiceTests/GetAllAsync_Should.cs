﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Email;

namespace VirtualCarGarage.Tests.UserServiceTests
{
    [TestClass]
    public class GetAllAsync_Should
    {
        private readonly IEmailService emailService;

        [TestMethod]
        public async Task ReturnCorrectAllUsers()
        {

            var options = Utils.GetOptions(nameof(ReturnCorrectAllUsers));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(Utils.GetUsers());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new UserService(actContext, emailService);
                var actual = sut.GetAllAsync();
                var expected = await actContext.Users.ToListAsync();
                Assert.AreEqual(actual.Result.Count, expected.Count);
                Assert.AreEqual(actual.Result.Count, 3);
            }
        }

        [TestMethod]
        public async Task ThrowWhen_NoUsersFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_NoUsersFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new UserService(actContext, emailService);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAllAsync());
            }
        }
    }
}
