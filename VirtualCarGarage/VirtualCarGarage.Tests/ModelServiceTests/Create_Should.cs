﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Tests.ModelServiceTests
{
    [TestClass]
    public class Create_Should
    {
        [TestMethod]
        public async Task SuccessfullyCreateModel()
        {
            var options = Utils.GetOptions(nameof(SuccessfullyCreateModel));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Manufacturers.AddRangeAsync(Utils.GetManufacturers());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ModelService(actContext);
                await sut.CreateAsync(new ModelDTO
                {
                    Name = "A6 Allroad",
                    ManufacturerId = 1
                });
                Assert.AreEqual(actContext.Models.Count(), 1);
                var actual = actContext.Models.First();
                Assert.AreEqual(actual.Name, "A6 Allroad");
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ModelAlreadyExists()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ModelAlreadyExists));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Manufacturers.AddRangeAsync(Utils.GetManufacturers());
                await arrangeContext.Models.AddRangeAsync(Utils.GetModels());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ModelService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync(new ModelDTO
                {
                    Name = "A6 Allroad",
                    ManufacturerId = 1
                }));
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ManufacturerDoesNotExist()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ManufacturerDoesNotExist));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ModelService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync(new ModelDTO
                {
                    Name = "A6 Allroad",
                    ManufacturerId = 1
                }));
            }
        }
    }
}
