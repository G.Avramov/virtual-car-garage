﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.ModelServiceTests
{
    [TestClass]
    public class GetAllAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectModelCount()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectModelCount));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Models.AddRangeAsync(Utils.GetModels());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ModelService(actContext);
                Assert.AreEqual(6, sut.GetAllAsync().Result.Count());
            }
        }
        [TestMethod]
        public async Task ThrowWhen_NoModelsFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_NoModelsFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ModelService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAllAsync());
            }
        }
    }
}
