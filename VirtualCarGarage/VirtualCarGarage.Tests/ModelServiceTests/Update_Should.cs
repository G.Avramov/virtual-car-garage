﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Tests.ModelServiceTests
{
    [TestClass]
    public class Update_Should
    {
        [TestMethod]
        public async Task CorrectlyUpdateModel()
        {
            var options = Utils.GetOptions(nameof(CorrectlyUpdateModel));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Models.AddRangeAsync(Utils.GetModels());
                await arrangeContext.Manufacturers.AddRangeAsync(Utils.GetManufacturers());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ModelService(actContext);
                await sut.UpdateAsync(new ModelDTO
                {
                    Id = 1,
                    Name = "A3"
                });
                var actual = actContext.Models.FirstOrDefault(s => s.ModelId == 1);
                Assert.AreEqual(actual.Name, "A3");
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ModelNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ModelNotFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ModelService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateAsync(new ModelDTO
                {
                    Id = 1,
                    Name = "A3"
                }));
            }
        }
    }
}
