﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.ModelServiceTests
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectModel()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectModel));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Models.AddRangeAsync(Utils.GetModels());
                await arrangeContext.Manufacturers.AddRangeAsync(Utils.GetManufacturers());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ModelService(actContext);
                var actual = sut.GetAsync(1);
                var expected = await actContext.Models.FirstOrDefaultAsync(s => s.ModelId == 1);
                Assert.AreEqual(actual.Result.Name, expected.Name);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ModelNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ModelNotFound));

            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ModelService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAsync(1));
            }
        }
    }
}
