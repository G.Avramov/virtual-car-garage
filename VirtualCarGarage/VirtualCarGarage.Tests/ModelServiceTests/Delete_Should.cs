﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.ModelServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public async Task CorrectlyDeleteModel()
        {
            var options = Utils.GetOptions(nameof(CorrectlyDeleteModel));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Models.AddRangeAsync(Utils.GetModels());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ModelService(actContext);
                await sut.DeleteAsync(1);
                var actual = actContext.Models.FirstOrDefault(s => s.ModelId == 1);
                Assert.AreEqual(actual.IsDeleted, true);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ModelNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ModelNotFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ModelService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.DeleteAsync(1));
            }
        }
    }
}
