﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Tests.CarServiceTests
{
    [TestClass]
    public class UpdateAsync_Should
    {
        [TestMethod]
        public async Task CorrectlyUpdateCar()
        {
            var options = Utils.GetOptions(nameof(CorrectlyUpdateCar));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Manufacturers.AddRangeAsync(Utils.GetManufacturers());
                await arrangeContext.Models.AddRangeAsync(Utils.GetModels());
                await arrangeContext.Users.AddRangeAsync(Utils.GetUsers());
                await arrangeContext.ServiceHistories.AddRangeAsync(Utils.GetServiceHistories());
                await arrangeContext.Cars.AddRangeAsync(Utils.GetCars());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new CarService(actContext);
                var car = await sut.UpdateAsync(new CarDTO
                {
                    Id = 1,
                    LicensePlate = "CA7574XB",
                    ManufacturerId = 1,
                    ModelId = 1,
                    UserId = 1,
                    VehicleIDNumber = "12345678901234567",
                });
                await actContext.SaveChangesAsync();
                var actual = actContext.Cars.FirstOrDefault();
                Assert.AreEqual(actual.LicensePlate, "CA7574XB");
                Assert.AreEqual(actual.VehicleIDNumber, "12345678901234567");
            }
        }
        [TestMethod]
        public async Task ThrowWhen_CarNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_CarNotFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new CarService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateAsync(new CarDTO()));
            }
        }
        [TestMethod]
        public async Task ThrowWhen_UserNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_UserNotFound));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Cars.AddRangeAsync(Utils.GetCars());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new CarService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateAsync(new CarDTO { Id = 1 }));
            }
        }
    }
}
