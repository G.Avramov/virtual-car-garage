﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.CarServiceTests
{
    [TestClass]
    public class DeleteAsync_Should
    {
        [TestMethod]
        public async Task SuccessfullyDeleteVehicle()
        {
            var options = Utils.GetOptions(nameof(SuccessfullyDeleteVehicle));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Cars.AddRangeAsync(Utils.GetCars());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new CarService(actContext);
                var car = actContext.Cars.FirstOrDefault(c => c.CarId == 1);
                await sut.DeleteAsync(car.CarId);
                Assert.AreEqual(car.IsDeleted, true);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_CarNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_CarNotFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new CarService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.DeleteAsync(1));
            }
        }
        [TestMethod]
        public async Task ThrowWhen_CarAlreadyDeleted()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_CarAlreadyDeleted));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Cars.AddRangeAsync(Utils.GetCars());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new CarService(actContext);
                var car = actContext.Cars.FirstOrDefault(c => c.CarId == 1);
                await sut.DeleteAsync(car.CarId);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.DeleteAsync(1));
            }
        }
    }
}
