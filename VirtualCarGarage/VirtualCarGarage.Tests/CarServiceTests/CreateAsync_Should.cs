﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Tests.CarServiceTests
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public async Task SuccessfullyCreateNewCar()
        {
            var options = Utils.GetOptions(nameof(SuccessfullyCreateNewCar));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Manufacturers.AddRangeAsync(Utils.GetManufacturers());
                await arrangeContext.Models.AddRangeAsync(Utils.GetModels());
                await arrangeContext.Users.AddRangeAsync(Utils.GetUsers());
                await arrangeContext.ServiceHistories.AddRangeAsync(Utils.GetServiceHistories());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new CarService(actContext);
                var car = await sut.CreateAsync(new CarDTO
                {
                    LicensePlate = "CA7574XB",
                    ManufacturerId = 1,
                    ModelId = 1,
                    UserId = 1,
                    VehicleIDNumber = "12345678901234567",
                });
                Assert.AreEqual(actContext.Cars.Count(), 1);
                var actual = actContext.Cars.FirstOrDefault();
            }
        }
        [TestMethod]
        public async Task ThrowWhen_UserNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_UserNotFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new CarService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync(new CarDTO
                {
                    LicensePlate = "CA7574XB",
                    ManufacturerId = 1,
                    ModelId = 1,
                    UserId = 1,
                    VehicleIDNumber = "12345678901234567",
                }));
            }
        }
        [TestMethod]
        public async Task ThrowWhen_CarIDAlreadyExists()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_CarIDAlreadyExists));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(Utils.GetUsers());
                await arrangeContext.Cars.AddRangeAsync(Utils.GetCars());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new CarService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync(new CarDTO
                {
                    LicensePlate = "CA7574XB",
                    ManufacturerId = 1,
                    ModelId = 1,
                    UserId = 1,
                    VehicleIDNumber = "1G6DP5E37D0821067",
                }));
            }
        }
    }
}
