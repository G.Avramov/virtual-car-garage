﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.CarServiceTests
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectCar()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectCar));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Models.AddRangeAsync(Utils.GetModels());
                await arrangeContext.Manufacturers.AddRangeAsync(Utils.GetManufacturers());
                await arrangeContext.Cars.AddRangeAsync(Utils.GetCars());
                await arrangeContext.Users.AddRangeAsync(Utils.GetUsers());
                await arrangeContext.ServiceHistories.AddRangeAsync(Utils.GetServiceHistories());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new CarService(actContext);
                var actual = sut.GetAsync(1);
                var expected = await actContext.Cars.FirstOrDefaultAsync(s => s.CarId == 1);
                Assert.AreEqual(actual.Result.VehicleIDNumber, expected.VehicleIDNumber);
                Assert.AreEqual(actual.Result.LicensePlate, expected.LicensePlate);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_VehicleNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_VehicleNotFound));

            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new CarService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAsync(1));
            }
        }
    }
}
