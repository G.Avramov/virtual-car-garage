﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.CarServiceTests
{
    [TestClass]
    public class GetAllAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectCarsCount()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectCarsCount));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Users.AddRangeAsync(Utils.GetUsers());
                await arrangeContext.Models.AddRangeAsync(Utils.GetModels());
                await arrangeContext.Manufacturers.AddRangeAsync(Utils.GetManufacturers());
                await arrangeContext.Cars.AddRangeAsync(Utils.GetCars());
                await arrangeContext.ServiceHistories.AddRangeAsync(Utils.GetServiceHistories());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new CarService(actContext);
                Assert.AreEqual(3, sut.GetAllAsync().Result.Count());
            }
        }
        [TestMethod]
        public async Task ThrowWhen_NoVehicleFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_NoVehicleFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new CarService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAllAsync());
            }
        }
    }
}
