﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Tests.OrderServiceTests
{
    [TestClass]
    public class AddServices_Should
    {
        [TestMethod]
        public async Task CorrectlyAddServicesToOrderOrder()
        {
            var options = Utils.GetOptions(nameof(CorrectlyAddServicesToOrderOrder));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Orders.AddRangeAsync(Utils.GetOrders());
                await arrangeContext.ServiceHistories.AddRangeAsync(Utils.GetServiceHistories());
                await arrangeContext.Services.AddRangeAsync(Utils.GetServices());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new OrderService(actContext);
                List<ServiceDTO> servicesDTO = new List<ServiceDTO>();
                servicesDTO.Add(new ServiceDTO
                {
                    Id = 1,
                    Name = "Car Battery Replacement",
                    Price = 105.50m
                });
                await sut.AddServices(1, servicesDTO);
                var actual = actContext.Orders.FirstOrDefault(s => s.OrderId == 1);
                Assert.AreEqual(actual.ServicesOrdered.Count, 1);
                Assert.AreEqual(actual.ServicesOrdered.First().Name, "Car Battery Replacement");
            }
        }

        [TestMethod]
        public async Task ThrowWhen_OrderNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_OrderNotFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                List<ServiceDTO> servicesDTO = new List<ServiceDTO>();
                servicesDTO.Add(new ServiceDTO
                {
                    Id = 1,
                    Name = "Car Battery Replacement",
                    Price = 105.50m
                });
                var sut = new OrderService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.AddServices(1, servicesDTO));
            }
        }
    }
}
