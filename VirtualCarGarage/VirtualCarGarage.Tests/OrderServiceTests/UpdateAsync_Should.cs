﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Tests.OrderServiceTests
{
    [TestClass]
    public class UpdateAsync_Should
    {
        [TestMethod]
        public async Task CorrectlyUpdateOrder()
        {
            var options = Utils.GetOptions(nameof(CorrectlyUpdateOrder));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Orders.AddRangeAsync(Utils.GetOrders());
                await arrangeContext.ServiceHistories.AddRangeAsync(Utils.GetServiceHistories());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new OrderService(actContext);
                await sut.UpdateAsync(new OrderEditDTO
                {               
                    Id = 1,
                    ServiceHistoryId = 2,
                    OrderDate = DateTime.Now.AddDays(-2)
                });
                var actual = actContext.Orders.FirstOrDefault(s => s.OrderId == 1);
                Assert.AreEqual(actual.ServiceHistoryId, 2);
                Assert.AreEqual(actual.OrderDate.Date, DateTime.Now.AddDays(-2).Date);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_OrderNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_OrderNotFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new OrderService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateAsync(new OrderEditDTO
                {
                    Id = 1,
                    ServiceHistoryId = 2,
                    OrderDate = DateTime.Now
                }));
            }
        }

        [TestMethod]
        public async Task ThrowWhen_ServiceHistoryNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ServiceHistoryNotFound));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Orders.AddRangeAsync(Utils.GetOrders());
                await arrangeContext.ServiceHistories.AddRangeAsync(Utils.GetServiceHistories());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new OrderService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateAsync(new OrderEditDTO
                {
                    Id = 1,
                    ServiceHistoryId = 4,
                    OrderDate = DateTime.Now
                }));
            }
        }
    }
}
