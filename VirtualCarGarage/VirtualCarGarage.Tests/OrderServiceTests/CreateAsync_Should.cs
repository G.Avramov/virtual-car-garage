﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Tests.OrderServiceTests
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public async Task SuccessfullyCreateOrder()
        {
            var options = Utils.GetOptions(nameof(SuccessfullyCreateOrder));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                using (var arrangeContext = new VirtualCarGarageDbContext(options))
                {
                    await arrangeContext.ServiceHistories.AddRangeAsync(Utils.GetServiceHistories());
                    await arrangeContext.SaveChangesAsync();
                }
                var sut = new OrderService(actContext);
                await sut.CreateAsync(new OrderDTO
                {
                    Id = 1,
                    ServiceHistoryId = 1,
                    OrderDate = DateTime.Now
                });
                Assert.AreEqual(actContext.Orders.Count(), 1);
                var actual = actContext.Orders.First();
                Assert.AreEqual(actual.ServiceHistoryId, 1);
                Assert.AreEqual(actual.OrderId, 1);
                Assert.AreEqual(actual.OrderDate.Date, DateTime.Now.Date);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ServiceHistoryNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ServiceHistoryNotFound));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Services.AddRangeAsync(Utils.GetServices());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new OrderService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync(new OrderDTO
                {
                    Id = 1,
                    ServiceHistoryId = 1,
                    OrderDate = DateTime.Now.AddDays(-2)
                }));
            }
        }
    }
}
