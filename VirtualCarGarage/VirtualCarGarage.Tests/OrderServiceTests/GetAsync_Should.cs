﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.OrderServiceTests
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectOrder()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectOrder));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Orders.AddRangeAsync(Utils.GetOrders());
                await arrangeContext.ServiceHistories.AddRangeAsync(Utils.GetServiceHistories());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new OrderService(actContext);
                var actual = sut.GetAsync(1);
                var expected = await actContext.Orders.FirstOrDefaultAsync(o => o.OrderId == 1);
                Assert.AreEqual(actual.Result.Id, expected.OrderId);
                Assert.AreEqual(actual.Result.ServiceHistoryId, expected.ServiceHistoryId);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_OrderNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_OrderNotFound));

            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new OrderService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAsync(1));
            }
        }
    }
}
