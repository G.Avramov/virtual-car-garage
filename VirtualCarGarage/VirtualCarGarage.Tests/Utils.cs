﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Data.Models;

namespace VirtualCarGarage.Tests
{
    public class Utils
    {
        public static DbContextOptions<VirtualCarGarageDbContext> GetOptions(string databaseName)
        {
            return new DbContextOptionsBuilder<VirtualCarGarageDbContext>()
                .UseInMemoryDatabase(databaseName)
                 .Options;
        }
        public static UserManager<TUser> TestUserManager<TUser>(IUserStore<TUser> store = null) where TUser : class
        {
            store = store ?? new Mock<IUserStore<TUser>>().Object;
            var options = new Mock<IOptions<IdentityOptions>>();
            var idOptions = new IdentityOptions();
            idOptions.Lockout.AllowedForNewUsers = false;
            options.Setup(o => o.Value).Returns(idOptions);
            var userValidators = new List<IUserValidator<TUser>>();
            var validator = new Mock<IUserValidator<TUser>>();
            userValidators.Add(validator.Object);
            var pwdValidators = new List<PasswordValidator<TUser>>();
            pwdValidators.Add(new PasswordValidator<TUser>());
            var userManager = new UserManager<TUser>(store, options.Object, new PasswordHasher<TUser>(),
                userValidators, pwdValidators, new UpperInvariantLookupNormalizer(),
                new IdentityErrorDescriber(), null,
                new Mock<ILogger<UserManager<TUser>>>().Object);
            validator.Setup(v => v.ValidateAsync(userManager, It.IsAny<TUser>()))
                .Returns(Task.FromResult(IdentityResult.Success)).Verifiable();
            return userManager;
        }

        public static List<Manufacturer> GetManufacturers()
        {
            return new List<Manufacturer>()
            {
                new Manufacturer()
                {
                    ManufacturerId = 1,
                    Name = "Audi"
                },
                new Manufacturer()
                {
                    ManufacturerId = 2,
                    Name = "Infiniti"
                },
                new Manufacturer()
                {
                    ManufacturerId = 3,
                    Name = "Volkswagen"
                }
            };
        }

        public static List<Model> GetModels()
        {
            return new List<Model>()
            {
                new Model()
                {
                    ModelId = 1,
                    Name = "A6 Allroad",
                    ManufacturerId = 1
                },
                new Model()
                {
                    ModelId = 2,
                    Name = "QX",
                    ManufacturerId = 2
                },
                new Model()
                {
                    ModelId = 3,
                    Name = "Passat Alltrack",
                    ManufacturerId = 3
                },
                new Model()
                {
                    ModelId = 4,
                    Name = "Q7",
                    ManufacturerId = 1
                },
                new Model()
                {
                    ModelId = 5,
                    Name = "FX",
                    ManufacturerId = 2
                },
                new Model()
                {
                    ModelId = 6,
                    Name = "Scirocco",
                    ManufacturerId = 3
                }
            };
        }

        public static List<Service> GetServices()
        {
            return new List<Service>()
            {
                new Service()
                {
                    ServiceId = 1,
                    Name = "Car Battery Replacement",
                    Price = 105.50m
                },
                new Service()
                {
                    ServiceId = 2,
                    Name = "Fuel Filter Replacement",
                    Price = 205.50m
                },
                new Service()
                {
                    ServiceId = 3,
                    Name = "Tire Rotation",
                    Price = 65.50m
                }
            };
        }

        public static List<User> GetUsers()
        {
            return new List<User>()
            {
                new User()
                {
                    Email = "test1@gmail.com",
                    PasswordHash = "0123456789",
                    FirstName = "Arthur",
                    LastName = "Pallent",
                    PhoneNumber = "7974107197"
                },
                new User()
                {
                    Email = "test2@gmail.com",
                    PasswordHash = "4563457568",
                    FirstName = "Sim",
                    LastName = "Oldaker",
                    PhoneNumber = "8888844444"
                },
                new User()
                {
                    Email = "test3@gmail.com",
                    PasswordHash = "56565656",
                    FirstName = "Myrvyn",
                    LastName = "Petschelt",
                    PhoneNumber = "6689830676"
                }
            };
        }

        public static List<Car> GetCars()
        {
            return new List<Car>()
            {
                new Car()
                {
                    CarId = 1,
                    UserId = 2,
                    ModelId = 3,
                    LicensePlate = "TC6785XX",
                    VehicleIDNumber = "1G6DP5E37D0821067"
                },
                new Car()
                {
                    CarId = 2,
                    UserId = 2,
                    ModelId = 5,
                    LicensePlate = "YX0437XX",
                    VehicleIDNumber = "WAUHFAFL5FN492123"
                },
                new Car()
                {
                    CarId = 3,
                    UserId = 2,
                    ModelId = 6,
                    LicensePlate = "TX7508TX",
                    VehicleIDNumber = "WBAAV53491J186647"
                }
            };
        }

        public static List<ServiceHistory> GetServiceHistories()
        {
            return new List<ServiceHistory>()
            {
                new ServiceHistory()
                {
                    ServiceHistoryId = 1,
                    CarId = 1
                },
               new ServiceHistory()
                {
                    ServiceHistoryId = 2,
                    CarId = 2
                },
                new ServiceHistory()
                {
                    ServiceHistoryId = 3,
                    CarId = 3
                }
            };
        }

        public static List<Order> GetOrders()
        {
            return new List<Order>()
            {
                new Order()
                {
                    OrderId = 1,
                    ServiceHistoryId = 1,
                    OrderDate = DateTime.Now.AddDays(-2)
                    /* ServicesOrdered = new List<Service>
                     {
                         new Service()
                 {
                     ServiceId = 1,
                     Name = "Car Battery Replacement",
                     Price = 105.50m
                 },
                 new Service()
                 {
                     ServiceId = 2,
                     Name = "Fuel Filter Replacement",
                     Price = 205.50m
                 },
                 new Service()
                 {
                     ServiceId = 3,
                     Name = "Tire Rotation",
                     Price = 65.50m
                 }
                     }
                 }*/
                },
                new Order()
                {
                    OrderId = 2,
                    ServiceHistoryId = 2,
                    OrderDate = DateTime.Now.AddDays(-3)
                    /*ServicesOrdered = new List<Service>
                    {
                        new Service()
                {
                    ServiceId = 1,
                    Name = "Car Battery Replacement",
                    Price = 105.50m
                },
                new Service()
                {
                    ServiceId = 2,
                    Name = "Fuel Filter Replacement",
                    Price = 205.50m
                },
                new Service()
                {
                    ServiceId = 3,
                    Name = "Tire Rotation",
                    Price = 65.50m
                }
                    }
                }*/
                }
                ,
                 new Order()
                {
                    OrderId = 3,
                    ServiceHistoryId = 3,
                    OrderDate = DateTime.Now.AddDays(-4)
                   /* ServicesOrdered = new List<Service>
                    {
                        new Service()
                {
                    ServiceId = 1,
                    Name = "Car Battery Replacement",
                    Price = 105.50m
                },
                new Service()
                {
                    ServiceId = 2,
                    Name = "Fuel Filter Replacement",
                    Price = 205.50m
                },
                new Service()
                {
                    ServiceId = 3,
                    Name = "Tire Rotation",
                    Price = 65.50m
                }
                    }*/
                }
            };
        }
    }
}
