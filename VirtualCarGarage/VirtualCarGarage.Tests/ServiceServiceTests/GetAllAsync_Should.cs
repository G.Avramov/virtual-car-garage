﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.ServiceServiceTests
{
    [TestClass]
    public class GetAllAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectServiceCount()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectServiceCount));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Services.AddRangeAsync(Utils.GetServices());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceService(actContext);
                Assert.AreEqual(3, sut.GetAllAsync().Result.Count());
            }
        }
        [TestMethod]
        public async Task ThrowWhen_NoServicesFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_NoServicesFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAllAsync());
            }
        }
    }
}
