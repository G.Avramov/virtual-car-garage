﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.ServiceServiceTests
{
    [TestClass]
    public class DeleteAsync_Should
    {
        [TestMethod]
        public async Task CorrectlyDeleteService()
        {
            var options = Utils.GetOptions(nameof(CorrectlyDeleteService));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Services.AddRangeAsync(Utils.GetServices());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceService(actContext);
                await sut.DeleteAsync(1);
                var actual = actContext.Services.FirstOrDefault(s => s.ServiceId == 1);
                Assert.AreEqual(actual.IsDeleted, true);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ServiceNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ServiceNotFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.DeleteAsync(1));
            }
        }
    }
}
