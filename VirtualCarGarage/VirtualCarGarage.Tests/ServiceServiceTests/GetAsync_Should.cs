﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.ServiceServiceTests
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectService()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectService));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Services.AddRangeAsync(Utils.GetServices());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceService(actContext);
                var actual = sut.GetAsync(1);
                var expected = await actContext.Services.FirstOrDefaultAsync(s => s.ServiceId == 1);
                Assert.AreEqual(actual.Result.Name, expected.Name);
                Assert.AreEqual(actual.Result.Price, expected.Price);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ServiceNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ServiceNotFound));

            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAsync(1));
            }
        }
    }
}
