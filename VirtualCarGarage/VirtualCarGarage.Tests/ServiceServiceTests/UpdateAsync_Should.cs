﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Tests.ServiceServiceTests
{
    [TestClass]
    public class UpdateAsync_Should
    {
        [TestMethod]
        public async Task CorrectlyUpdateService()
        {
            var options = Utils.GetOptions(nameof(CorrectlyUpdateService));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Services.AddRangeAsync(Utils.GetServices());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceService(actContext);
                await sut.UpdateAsync(new ServiceDTO
                {
                    Id = 1,
                    Name = "Car Battery Test",
                    Price = 155.50m
                });
                var actual = actContext.Services.FirstOrDefault(s => s.ServiceId == 1);
                Assert.AreEqual(actual.Name, "Car Battery Test");
                Assert.AreEqual(actual.Price, 155.50m);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ServiceNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ServiceNotFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.UpdateAsync(new ServiceDTO
                {
                    Id = 1,
                    Name = "Car Battery Test",
                    Price = 155.50m
                }));
            }
        }
    }
}
