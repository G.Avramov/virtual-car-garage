﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Tests.ServiceServiceTests
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public async Task SuccessfullyCreateService()
        {
            var options = Utils.GetOptions(nameof(SuccessfullyCreateService));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceService(actContext);
                await sut.CreateAsync(new ServiceDTO
                {
                    Name = "Car Battery Test",
                    Price = 155.50m
                });
                Assert.AreEqual(actContext.Services.Count(), 1);
                var actual = actContext.Services.First();
                Assert.AreEqual(actual.Name, "Car Battery Test");
                Assert.AreEqual(actual.Price, 155.50m);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ServiceAlreadyExists()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ServiceAlreadyExists));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Services.AddRangeAsync(Utils.GetServices());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync(new ServiceDTO
                {
                    Name = "Car Battery Replacement",
                    Price = 105.50m
                }));
            }
        }
    }
}
