﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.ServiceHistoryServiceTests
{
    [TestClass]
    public class GetAsync_Should
    {
        [TestMethod]
        public async Task ReturnCorrectServiceHistory()
        {
            var options = Utils.GetOptions(nameof(ReturnCorrectServiceHistory));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Orders.AddRangeAsync(Utils.GetOrders());
                await arrangeContext.Users.AddRangeAsync(Utils.GetUsers());
                await arrangeContext.Services.AddRangeAsync(Utils.GetServices());
                await arrangeContext.Models.AddRangeAsync(Utils.GetModels());
                await arrangeContext.Manufacturers.AddRangeAsync(Utils.GetManufacturers());
                await arrangeContext.Cars.AddRangeAsync(Utils.GetCars());
                await arrangeContext.ServiceHistories.AddRangeAsync(Utils.GetServiceHistories());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceHistoryService(actContext);
                var actual = sut.GetAsync(1);
                var expected = await actContext.ServiceHistories.FirstOrDefaultAsync(sh => sh.ServiceHistoryId == 1);
                Assert.AreEqual(actual.Result.Id, expected.ServiceHistoryId);
                Assert.AreEqual(actual.Result.CarId, expected.CarId);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_ServiceHistoryNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_ServiceHistoryNotFound));

            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceHistoryService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.GetAsync(1));
            }
        }
    }
}
