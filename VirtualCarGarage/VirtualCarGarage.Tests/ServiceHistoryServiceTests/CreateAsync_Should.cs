﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;
using VirtualCarGarage.Services.Models;

namespace VirtualCarGarage.Tests.ServiceHistoryServiceTests
{
    [TestClass]
    public class CreateAsync_Should
    {
        [TestMethod]
        public async Task ThrowWhen_CarHasServiceHistory()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_CarHasServiceHistory));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                using (var arrangeContext = new VirtualCarGarageDbContext(options))
                {
                    await arrangeContext.Orders.AddRangeAsync(Utils.GetOrders());
                    await arrangeContext.Users.AddRangeAsync(Utils.GetUsers());
                    await arrangeContext.Services.AddRangeAsync(Utils.GetServices());
                    await arrangeContext.Models.AddRangeAsync(Utils.GetModels());
                    await arrangeContext.Manufacturers.AddRangeAsync(Utils.GetManufacturers());
                    await arrangeContext.Cars.AddRangeAsync(Utils.GetCars());
                    await arrangeContext.ServiceHistories.AddRangeAsync(Utils.GetServiceHistories());
                    await arrangeContext.SaveChangesAsync();
                }
                var sut = new ServiceHistoryService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync(new ServiceHistoryDTO
                {
                    Id = 1,
                    CarId = 1
                }));
            }
        }
        [TestMethod]
        public async Task ThrowWhen_CarNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_CarNotFound));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.Cars.AddRangeAsync(Utils.GetCars());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceHistoryService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.CreateAsync(new ServiceHistoryDTO
                {
                    Id = 1,
                    CarId = 10
                }));
            }
        }

        /*[TestMethod]
        public async Task SuccessfullyCreateServiceHistory()
        {
            
        }*/
    }
}
