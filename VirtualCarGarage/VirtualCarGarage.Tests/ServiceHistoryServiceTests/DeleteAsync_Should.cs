﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VirtualCarGarage.Data;
using VirtualCarGarage.Services;

namespace VirtualCarGarage.Tests.ServiceHistoryServiceTests
{
    [TestClass]
    public class DeleteAsync_Should
    {
        [TestMethod]
        public async Task CorrectlyDeleteServiceHistory()
        {
            var options = Utils.GetOptions(nameof(CorrectlyDeleteServiceHistory));
            using (var arrangeContext = new VirtualCarGarageDbContext(options))
            {
                await arrangeContext.ServiceHistories.AddRangeAsync(Utils.GetServiceHistories());
                await arrangeContext.SaveChangesAsync();
            }
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceHistoryService(actContext);
                await sut.DeleteAsync(1);
                var actual = actContext.ServiceHistories.FirstOrDefault(sh => sh.ServiceHistoryId == 1);
                Assert.AreEqual(actual.IsDeleted, true);
            }
        }
        [TestMethod]
        public async Task ThrowWhen_OrderNotFound()
        {
            var options = Utils.GetOptions(nameof(ThrowWhen_OrderNotFound));
            using (var actContext = new VirtualCarGarageDbContext(options))
            {
                var sut = new ServiceHistoryService(actContext);
                await Assert.ThrowsExceptionAsync<ArgumentException>(() => sut.DeleteAsync(1));
            }
        }
    }
}
