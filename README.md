# Virtual Car Garage

VIRTUAL CAR GARAGE enables the owners of car services to manage their day-to-day operations. 
It has a list with cars (manufacturer, model, year) and all available services (oil change, 
filters change etc.) and their price. The system has a list with all customers and their 
contact information (Name, telephone, email etc.). Admins are able to link specific car/cars to 
customer. Each specific car has some identification details (registration plate number, 
VIN etc.). 
VIRTUAL CAR GARAGE keeps history of all services done on a customer’s cars. For all customers, а 
profile can be generated providing them with access to their personal information via the web 
UI. 
There is no registration form! To become a customer, one has to first contact VIRTUAL CAR 
GARAGE via a contact form on the site and provide an email for registration.
An administrator from VIRTUAL CAR GARAGE then generates login credentials and sends them 
back to the provided email.


**Database diagram:**

![image](/uploads/912c9df03214d2b96e97feb270521026/image.png)

**Technologies:**

- ASP.NET 5

- ASP.NET Identity

- Entity Framework Core 5

- MS SQL Server

- HTML

- Bootstrap
